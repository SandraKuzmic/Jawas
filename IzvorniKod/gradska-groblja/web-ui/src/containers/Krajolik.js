import React, {Component} from "react";
import {connect} from "react-redux";
import krajolikActionCreator from "./../actionCreators/krajolikActionCreator";
import Header from '../components/Header';
import MuzejUvodniZaslon from '../components/MuzejPageComponents/MuzejUvodniZaslon';
import ElementMuzeja from '../components/MuzejPageComponents/ElementMuzeja';
import Footer from '../components/Footer';
import './Krajolik.css';
import $ from 'jquery';
import licnostActionCreator from "../actionCreators/licnostActionCreator";
import grobljeActionCreator from "../actionCreators/grobljeActionCreator";

class Krajolik extends Component {
    constructor(props) {
        super(props);

        this.state = {
            grobljeId: this.props.match.params.grobljeId
        };

        this.activeListKrajobrazi = this.activeListKrajobrazi.bind(this);
        this.activeListLicnosti = this.activeListLicnosti.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(krajolikActionCreator.fetchKrajolike());
        this.props.dispatch(licnostActionCreator.fetchLicnosti());
        this.props.dispatch(grobljeActionCreator.fetchGroblje(this.props.match.params.grobljeId))
    }

    activeListKrajobrazi() {
            $(".licnosti").removeClass('active');
            $(".licnosti-header").removeClass('active');

            $(".krajobrazi").addClass('active');
            $(".krajobrazi-header").addClass('active');

        }

    activeListLicnosti() {
            $(".krajobrazi").removeClass('active');
            $(".krajobrazi-header").removeClass('active');

            $(".licnosti").addClass('active');
            $(".licnosti-header").addClass('active');
    }

    render() {

        let krajolici = [];
        if (this.props.krajolici) {
            krajolici = this.props.krajolici
                .filter(x => this.props.groblje.naziv === x.lokacija)
                .map((x) => (
                <div key={x.id} className=" col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div className="element-muzeja">
                        <ElementMuzeja element={x} isLicnost={false}/>
                    </div>
                </div>
            ));
        }

        let licnosti = [];
        if (this.props.licnosti) {
            licnosti = this.props.licnosti
                .filter(x => this.props.groblje.id === x.grobnoMjesto.groblje.id)
                .map((x) => (
                <div key={x.id} className=" col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div className="element-muzeja">
                        <ElementMuzeja element={x} isLicnost={true}/>
                    </div>
                </div>
            ));
        }

        return (
            <div>
                <Header/>
                <MuzejUvodniZaslon groblje={this.props.groblje}/>

                <div id="museum" className="oboje">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12 flex-col">
                                <h2 className="krajobrazi-header active" onClick={this.activeListKrajobrazi}>Krajolik</h2>
                                <h2 className="licnosti-header" onClick={this.activeListLicnosti}>Ličnosti</h2>
                            </div>
                            <div className="col-sm-12 flex-col">
                                <div className="muzej-lista krajobrazi active">
                                    <div className="lista">
                                        {krajolici}
                                    </div>
                                </div>
                                <div className="muzej-lista licnosti">
                                    <div className="lista">
                                        {licnosti}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {krajolici: state.krajolici.all, licnosti: state.licnosti.all, groblje: state.groblja.current}
};

export default connect(mapStateToProps)(Krajolik);