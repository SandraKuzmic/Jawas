import React, {Component} from "react";
import {connect} from "react-redux";
import KorisnikUslugaGrobnoMjesto from "./../components/KorisnikUslugaGrobnoMjesto";
import korisnikUslugaGrobnoMjestoActionCreator from "./../actionCreators/korisnikUslugaGrobnoMjestoActionCreator";

class KorisnikUslugaGrobnoMjestoList extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(korisnikUslugaGrobnoMjestoActionCreator.fetchKorisniciUslugaGrobnoMjesto());
    }

    render() {
        let korisniciUslugaGrobnoMjesto = [];
        if (this.props.korisniciUslugaGrobnoMjesto) {
            korisniciUslugaGrobnoMjesto = this.props.korisniciUslugaGrobnoMjesto.all.map((x) => <KorisnikUslugaGrobnoMjesto key={x.id} korisniciUslugaGrobnoMjesto={x}/>);
        }

        return (
            <div>
                <h1>Korisnik Usluga Grobno mjesto</h1>
                <ul>
                    {korisniciUslugaGrobnoMjesto}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {korisniciUslugaGrobnoMjesto: state.korisniciUslugaGrobnoMjesto}
};

export default connect(mapStateToProps)(KorisnikUslugaGrobnoMjestoList);