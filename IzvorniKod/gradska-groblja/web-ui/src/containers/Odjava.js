import React, {Component} from "react";
import { Redirect } from "react-router-dom";
import {connect} from "react-redux";

import paths from '../constants/paths';
import sessionActionCreator from "./../actionCreators/sessionActionCreator";

class Odjava extends Component {

    componentDidMount() {
        this.props.dispatch(sessionActionCreator.odjavaKorisnika());
    }
    
    render() {
        return (
            <div>
                <Redirect to={{
                    pathname: paths.routes.pocetna,
                }} />
            </div>
        );
    }
}

export default connect()(Odjava);