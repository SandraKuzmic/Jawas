import React, {Component} from "react";
import {connect} from "react-redux";
import Pokojnik from "./../components/Pokojnik";
import pokojnikActionCreator from "../actionCreators/pokojnikActionCreator";

class PokojnikList extends Component {
    constructor (props) {
        super(props);
        this.props.dispatch(pokojnikActionCreator.fetchPokojnike());
    }

    render () {
        let pokojnici = [];
        if (this.props.pokojnici) {
            pokojnici = this.props.pokojnici.all.map((x) => <Pokojnik key={x.id} pokojnik = {x}/>);
        }

        return (
            <div>
                <h1>Pokojnik</h1>
                <ul>
                    {pokojnici}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {pokojnici: state.pokojnici}
};

export default connect(mapStateToProps)(PokojnikList);