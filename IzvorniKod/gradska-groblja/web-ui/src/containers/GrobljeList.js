import React, {Component} from "react";
import {connect} from "react-redux";
import Groblje from "./../components/Groblje";
import Header from "./../components/Header";
import Footer from '../components/Footer';
import UvodniZaslon from '../components/GrobljePageComponents/UvodniZaslon';
import groblje from "./../static/groblje.jpg"

class GrobljeList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let groblja = [];
        if (this.props.groblja) {
            groblja = this.props.groblja.all.map((x) => <Groblje key={x.id} groblje={x}/>);
        }

        return (
            <div>
                <div>
                    <Header/>
                    <UvodniZaslon naziv="Gradska groblja" slika={groblje}/>
                    <ul className="groblje-list">
                        {groblja}
                    </ul>
                    <Footer/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {groblja: state.groblja}
};

export default connect(mapStateToProps)(GrobljeList);