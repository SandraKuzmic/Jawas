import React, {Component} from "react";
import { Redirect } from "react-router-dom";
import {connect} from "react-redux";

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';

import "./Registracija.css";
import Header from '../components/Header';
import paths from '../constants/paths';
import sessionActionCreator from "./../actionCreators/sessionActionCreator";

import { required, oib } from "../utils/validators";

class Registracija extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
        };
    }

    handleSubmit = event => {
        event.preventDefault();
        const values = this.form.getValues();

        this.props.dispatch((sessionActionCreator.registracijaKorisnika(values, (error) => {
            this.setState((state) => ({ error }));
        })));
    }

    render() {
        if (this.props.token) {
            return (
                <Redirect to={{
                    pathname: paths.routes.pocetna,
                }} />
            )
        };
        
        const error = this.state.error && (
            <span className="help-block">
                { this.state.error }
            </span>
        );

        return (
            <div>
                <Header />
                <div className="registration-container">
                    <h1>Registracija</h1>

                    <Form ref={ (c) => { this.form = c; }} onSubmit={this.handleSubmit}>
                        { error }

                        <div className="form-group">
                            <label htmlFor="oib">OIB:</label>
                            <Input className="form-control" type="text" name="oib" placeholder="01234567890" validations={[ required, oib ]}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="ime">Ime:</label>
                            <Input className="form-control" type="text" name="ime" validations={[ required ]}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="prezime">Prezime:</label>
                            <Input className="form-control" type="text" name="prezime" validations={[ required ]}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="korisnickoIme">Korisničko ime:</label>
                            <Input className="form-control" type="text" name="korisnickoIme" validations={[ required ]}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="lozinka">Lozinka:</label>
                            <Input className="form-control" type="password" name="lozinka" validations={[ required ]}/>
                        </div>

                        <div className="form-group">
                            <Button className="btn btn-more" type="submit">Registracija</Button>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { token: state.session.token }
};

export default connect(mapStateToProps)(Registracija);