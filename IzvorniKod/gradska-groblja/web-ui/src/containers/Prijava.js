import React, {Component} from "react";
import { Redirect, Link } from "react-router-dom";
import {connect} from "react-redux";

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';

import "./Prijava.css";
import Header from '../components/Header';
import paths from '../constants/paths';
import sessionActionCreator from "./../actionCreators/sessionActionCreator";

import { required } from "../utils/validators";

class Prijava extends Component {

    constructor() {
        super();
        this.state = {
            error: null,
        };
    }
    
    handleSubmit = event => {
        event.preventDefault();
        const values = this.form.getValues();

        this.props.dispatch(sessionActionCreator.prijavaKorisnika(values, (error) => {
            this.setState((state) => ({ error }));
        }));
    };

    render() {
        if (this.props.token) {
            return (
                <Redirect to={{
                    pathname: paths.routes.pocetna,
                }} />
            )
        }

        const error = this.state.error && (
            <span className="help-block">
                { this.state.error }
            </span>
        );

        return (
            <div>
                <Header />
                <div className="login-container">
                    <h1>Prijava</h1>

                    <Form ref={ (c) => { this.form = c; }} onSubmit={this.handleSubmit}>
                        { error }
                        
                        <div className="form-group">
                            <label htmlFor="korisnickoIme">Korisničko ime:</label>
                            <Input className="form-control" type="text" name="korisnickoIme" validations={[ required ]}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="lozinka">Lozinka:</label>
                            <Input className="form-control" type="password" name="lozinka" validations={[ required ]}/>
                        </div>

                        <div>
                            <Button className="btn btn-more" type="submit">Prijava</Button>
                            <div>Nemate račun?&nbsp;<Link to={paths.routes.registracija}>Registrirajte se!</Link></div>
                        </div>
                </Form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { token: state.session.token }
};

export default connect(mapStateToProps)(Prijava);