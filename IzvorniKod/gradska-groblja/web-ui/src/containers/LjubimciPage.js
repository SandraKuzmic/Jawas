import React, {Component} from "react";
import KorisnikoviLjubimci from '../components/LjubimciPageComponents/KorisnikoviLjubimci';
import Header from '../components/Header';
import OstaliLjubimci from '../components/LjubimciPageComponents/OstaliLjubimci';
import Ljubimac from "../components/LjubimciPageComponents/Ljubimac";
import Footer from '../components/Footer';
import {connect} from "react-redux";

class LjubimciPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let ljubimci;

        if (this.props.ljubimci) {
            if (this.props.isLoggedIn && this.props.korisnik) {
                let vlasnik = this.props.korisnik.ime + " " + this.props.korisnik.prezime;
                ljubimci = this.props.ljubimci
                    .filter(x => x.privatnost !== "SAMO_VLASNIK" && x.vlasnikImePrezime !== vlasnik)
                    .map((x) => <Ljubimac key={x.id} ljubimac={x}/>);
            } else {
                ljubimci = this.props.ljubimci
                    .filter(x => x.privatnost === "JAVNO")
                    .map((x) => <Ljubimac key={x.id} ljubimac={x}/>);
            }
        }

        return (
            <div>
                <Header/>
                <KorisnikoviLjubimci/>
                <OstaliLjubimci list={ljubimci} isLoggedIn={this.props.isLoggedIn}/>
                <Footer/>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {ljubimci: state.ljubimci.all, isLoggedIn: !!state.session.token, korisnik: state.session.korisnik}
};


export default connect(mapStateToProps)(LjubimciPage);