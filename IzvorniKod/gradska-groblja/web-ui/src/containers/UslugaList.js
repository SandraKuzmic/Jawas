import React, {Component} from "react";
import {connect} from "react-redux";
import Usluga from "./../components/Usluga";
import uslugaActionCreator from "./../actionCreators/uslugaActionCreator";

class UslugaList extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(uslugaActionCreator.fetchUsluge());
    }

    render() {
        let usluge = [];
        if (this.props.usluge) {
            usluge = this.props.usluge.all.map((x) => <Usluga key={x.id} usluga={x}/>);
        }

        return (
            <div>
                <h1>Usluge</h1>
                <ul>
                    {usluge}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {usluge: state.usluge}
};

export default connect(mapStateToProps)(UslugaList);