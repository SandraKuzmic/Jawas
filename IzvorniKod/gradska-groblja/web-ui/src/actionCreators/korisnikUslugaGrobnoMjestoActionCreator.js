import request from "../utils/backend";
import paths from "../constants/paths";
import * as korisnikUslugaGrobnoMjestoAction from "../actions/korisnikUslugaGrobnoMjestoAction";

export default {
    fetchKorisniciUslugaGrobnoMjesto() {
        return dispatch => {
            request
                .get(paths.restApi.korisniciUslugaGrobnoMjesto)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(korisnikUslugaGrobnoMjestoAction.fetchKorisniciUslugaGrobnoMjesto({status: "failure", data: err}));
                    } else {
                        dispatch(korisnikUslugaGrobnoMjestoAction.fetchKorisniciUslugaGrobnoMjesto({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchKorisnikUslugaGrobnoMjesto(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchKorisnikUslugaGrobnoMjesto(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(korisnikUslugaGrobnoMjestoAction.fetchKorisnikUslugaGrobnoMjesto({status: "failure", data: err}));
                    } else {
                        dispatch(korisnikUslugaGrobnoMjestoAction.fetchKorisnikUslugaGrobnoMjesto({status: "success", data: res.body}));
                    }
                });
        }
    }
}