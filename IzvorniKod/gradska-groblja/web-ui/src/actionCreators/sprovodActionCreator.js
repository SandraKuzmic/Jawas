import request from "../utils/backend";
import paths from "../constants/paths";
import * as sprovodAction from "../actions/sprovodAction";

export default {
    fetchSprovode() {
        return dispatch => {
            request
                .get(paths.restApi.sprovodi)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(sprovodAction.fetchSprovode({status: "failure", data: err}));
                    } else {
                        dispatch(sprovodAction.fetchSprovode({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchSprovod(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchSprovod(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(sprovodAction.fetchSprovod({status: "failure", data: err}));
                    } else {
                        dispatch(sprovodAction.fetchSprovod({status: "success", data: res.body}));
                    }
                });
        }
    }
}