import request from "../utils/backend";
import paths from "../constants/paths";
import * as grobnoMjestoAction from "../actions/grobnoMjestoAction";

export default {
    fetchGrobnaMjesta() {
        return dispatch => {
            request
                .get(paths.restApi.grobnaMjesta)
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(grobnoMjestoAction.fetchGrobnaMjesta({status: "failure", data: err}));
                    } else {
                        dispatch(grobnoMjestoAction.fetchGrobnaMjesta({status: "success", data: res.body}));
                    }
                });
        }
    },

    fetchGrobnoMjesto(id) {
        return dispatch => {
            request
                .get(paths.restApi.fetchGrobnoMjesto(id))
                .accept("application/json")
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(grobnoMjestoAction.fetchGrobnoMjesto({status: "failure", data: err}));
                    } else {
                        dispatch(grobnoMjestoAction.fetchGrobnoMjesto({status: "success", data: res.body}));
                    }
                });
        }
    }
}