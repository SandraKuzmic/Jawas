import request from "../utils/backend";
import paths from "../constants/paths";
import * as sessionAction from "../actions/sessionAction";

export default {
    registracijaKorisnika(data, errorCallback) {
        return dispatch => {
            request
                .post(paths.restApi.registracija, data)
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(sessionAction.postRegistracija({ error: err, token: null, korisnik: null }));
                        errorCallback(err.response.text);
                    } else {
                        dispatch(sessionAction.postRegistracija({ error: null, token: res.body.token, korisnik: res.body.korisnik }));
                    }
                });
        }
    },
    prijavaKorisnika(data, errorCallback) {
        return dispatch => {
            request
                .post(paths.restApi.prijava, data)
                .end((err, res) => {
                    if (err || !res.ok) {
                        dispatch(sessionAction.postPrijava({ error: err, token: null, korisnik: null }));
                        errorCallback(err.response.text);
                    } else {
                        dispatch(sessionAction.postPrijava({ error: null, token: res.body.token, korisnik: res.body.korisnik }));
                    }
                });
        }
    },
    odjavaKorisnika: () => sessionAction.noneOdjava,
}