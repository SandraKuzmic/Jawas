import React from "react";
import "./Licnost.css";

const Licnost = (props) => {
    return (
        <div>
            <li>{props.licnost.ime},
                {props.licnost.prezime},
                {props.licnost.grobnoMjesto},
                {props.licnost.slika},
                {props.licnost.opis}
            </li>
            <br/>
        </div>
    );
};

export default Licnost;