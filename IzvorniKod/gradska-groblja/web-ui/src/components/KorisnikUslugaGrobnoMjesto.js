import React from "react";
import "./KorisnikUslugaGrobnoMjesto.css";

const KorisnikUslugaGrobnoMjesto = (props) => {
    return (
        <div>
            <li>{props.korisnikUslugaGrobnoMjesto}
            </li>
            <br/>
        </div>
    );
};

export default KorisnikUslugaGrobnoMjesto;