import React from "react";
import {Route, Switch} from "react-router-dom";
import paths from "../constants/paths";
import HomePage from "../containers/HomePage";
import KorisnikList from "../containers/KorisnikList";
import LjubimciPage from "../containers/LjubimciPage";
import GrobljePage from "../containers/GrobljePage";
import GrobljeList from "../containers/GrobljeList";
import ErrorPage from "./ErrorPage";
import "./Main.css";
import KrajolikList from "../containers/KrajolikList";
import Krajolik from "../containers/Krajolik";
import Registracija from "../containers/Registracija";
import Prijava from "../containers/Prijava";
import Odjava from "../containers/Odjava";
import DataInit from "./DataInit";

const Main = () => (
    <main>
        <DataInit/>
        <Switch>
            <Route exact path={paths.routes.pocetna} component={HomePage}/>
            <Route exact path={paths.routes.grobljeList} component={GrobljeList}/>
            <Route exact path={paths.routes.groblje(":grobljeId")} component={GrobljePage}/>
            <Route exact path={paths.routes.krajolikList} component={KrajolikList}/>
            <Route exact path={paths.routes.krajolik(":grobljeId")} component={Krajolik}/>
            <Route exact path={paths.routes.ljubimacList} component={LjubimciPage}/>
            <Route exact path={paths.routes.admin} component={KorisnikList}/>
            <Route exact path={paths.routes.registracija} component={Registracija}/>
            <Route exact path={paths.routes.prijava} component={Prijava}/>
            <Route exact path={paths.routes.odjava} component={Odjava}/>
            <Route component={ErrorPage}/>
        </Switch>
    </main>
);

export default Main;