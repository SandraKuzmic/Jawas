import React, {Component} from "react";
import info from "./../static/info.png"
import "./Pokojnik.css";
import Modal from "react-bootstrap/es/Modal";
import {formatDate} from "../utils/helpFunctions";

class Pokojnik extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModal: false
        };

        this.handleModalOpen = this.handleModalOpen.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    handleModalOpen() {
        this.setState({showModal: true});
    }

    handleModalClose() {
        this.setState({showModal: false});
    }

    render() {
        let datumRodenja = this.props.pokojnik.datumRodenja == null ? <div/>
            : <li>
                <div className="pokojinik-label">Datum rođenja</div>
                <div className="value">{formatDate(this.props.pokojnik.datumRodenja)}</div>
            </li>;

        let datumSmrti = this.props.pokojnik.datumSmrti == null ?
            <div/> :
            <li>
                <div className="pokojinik-label">Datum smrti</div>
                <div className="value">{formatDate(this.props.pokojnik.datumSmrti)}</div>
            </li>;

        let datumUkopa = this.props.pokojnik.datumUkopa == null ?
            <div/> :
            <li>
                <div className="pokojinik-label">Datum ukopa</div>
                <div className="value">{formatDate(this.props.pokojnik.datumUkopa)}</div>
            </li>;

        let isLicnost = this.props.pokojnik.opis != null || this.props.pokojnik.slika != null;

        let opis = this.props.pokojnik.opis == null ?
            <div/> :
            <li>
                <div className="pokojinik-label">Opis</div>
                <div className="value">{this.props.pokojnik.opis}</div>
            </li>;

        let slika = this.props.pokojnik.slika == null ?
            <div/> :
            <div className="modalLeft">
                <div className="slika-wrapper">
                    <img src={this.props.pokojnik.slika}/>
                </div>
            </div>;

        return (
            <div>
                <div className="fields">
                    <p>Odjel: {this.props.pokojnik.grobnoMjesto.odjel}</p>
                    <p>Polje: {this.props.pokojnik.grobnoMjesto.polje}</p>
                    <p>Razred: {this.props.pokojnik.grobnoMjesto.razred}</p>
                    <p>Broj: {this.props.pokojnik.grobnoMjesto.broj}</p>
                    <img className="info" src={info} alt="info" width="35" onClick={this.handleModalOpen}/>
                </div>

                <Modal show={this.state.showModal} onHide={this.handleModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Informacije o pokojniku
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="korisnikov-ljubimac modalWrap">
                            {slika}
                            {isLicnost ?
                                <div className="modalRight">
                                    <ul className="labels">
                                        <li>
                                            <div className="pokojinik-label">Ime i prezime</div>
                                            <div
                                                className="value">{this.props.pokojnik.ime + " " + this.props.pokojnik.prezime}</div>
                                        </li>
                                        {datumRodenja}
                                        {datumSmrti}
                                        {datumUkopa}
                                        <li>
                                            <div className="pokojinik-label">Grobno mjesto</div>
                                            <div className="value">{this.props.pokojnik.grobnoMjestoString}</div>
                                        </li>
                                        {opis}
                                    </ul>
                                </div>
                                :
                                <ul className="labels">
                                    <li>
                                        <div className="pokojinik-label">Ime i prezime</div>
                                        <div
                                            className="value">{this.props.pokojnik.ime + " " + this.props.pokojnik.prezime}</div>
                                    </li>
                                    {datumRodenja}
                                    {datumSmrti}
                                    {datumUkopa}
                                    <li>
                                        <div className="pokojinik-label">Grobno mjesto</div>
                                        <div className="value">{this.props.pokojnik.grobnoMjestoString}</div>
                                    </li>
                                </ul>
                            }
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default Pokojnik;