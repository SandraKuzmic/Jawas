import React, {Component} from 'react';
import {connect} from "react-redux";
import ObavijestItem from "../ObavijestItem";
import './Obavijesti.css';

class Obavijesti extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showMsg: false
        };

        this.showMessage = this.showMessage.bind(this);
    }

    showMessage() {
        let toggle = !this.state.showMsg;
        this.setState({showMsg: toggle});
    }

    render() {

        let obavijesti = [];

        if (this.props.obavijesti !== null) {
            obavijesti = this.props.obavijesti.map(x =>
                <ObavijestItem key={x.id} obavijest={x}/>
            )
        }

        let viseObavijesti = this.state.showMsg ?
            <p className="fields">Prikazane su sve obavijesti!</p> : "";

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 heading text-center"><h2>Obavijesti</h2></div>
                </div>
                <div className="row flex-row">
                    {obavijesti}
                </div>
                <div className="row">
                    <div className="col-md-12 text-center vise">
                        {viseObavijesti}
                        <button className="btn-more" onClick={this.showMessage}>Više obavijesti</button>
                    </div>
                </div>
            </div>
        );

    };
}

const mapStateToProps = (state) => {
    return {obavijesti: state.groblja.obavijesti}
};

export default connect(mapStateToProps)(Obavijesti);
