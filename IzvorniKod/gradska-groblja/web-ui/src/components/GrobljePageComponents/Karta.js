import React, {Component} from "react";
import {connect} from "react-redux";
import Pokojnik from "../Pokojnik";
import './Karta.css';

class Karta extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isStart: true,
            isEmptyForm: false,
            searchSuccess: false,
            searchRresult: null,
        };

        this.search = this.search.bind(this);
        this.checkIfNull = this.checkIfNull.bind(this);
    }

    validate(e) {
        e.preventDefault();

        let errors = 0;
        errors = this.checkIfNull(this.refs.ime, "ime", errors);
        errors = this.checkIfNull(this.refs.prezime, "prezime", errors);

        if (this.refs.ime.value.trim() === "" || this.refs.prezime.value.trim() === "") {
            errors++;
        }

        this.setState({isStart: false});

        if (!errors) {
            this.search();
        } else {
            this.setState({isEmptyForm: true});
        }

    }

    checkIfNull(el, id, errors) {
        if (!el.value) {
            document.getElementById(id).className += " required-field";
            return ++errors;
        }
        return errors;
    }


    search() {
        let ime = this.refs.ime.value.trim().toLowerCase();
        let prezime = this.refs.prezime.value.trim().toLowerCase();

        let dataExists = this.props.pokojnici.find(x => (x.ime.toLowerCase() === ime) && (x.prezime.toLowerCase() === prezime));
        if (dataExists === undefined) {
            this.setState({
                isEmptyForm: false,
                searchSuccess: false
            });
            return;
        }

        let searchResult = this.props.pokojnici.map(x => {
            if ((x.ime.toLowerCase() === ime) && (x.prezime.toLowerCase() === prezime)) {
                return <Pokojnik key={x.id} pokojnik={x}/>
            }
        });

        this.setState({
            isEmptyForm: false,
            searchSuccess: true,
            searchResult: searchResult,
        });
    };

    render() {

        let searchResult = [];
        if (this.state.isStart) {
            searchResult = <div/>
        } else if (this.state.isEmptyForm) {
            searchResult = <p className="fields">Unesite željeno ime i prezime</p>
        } else if (this.state.searchSuccess) {
            searchResult = this.state.searchResult;
        } else if (!this.state.searchSuccess) {
            searchResult = <p className="fields">Nema rezultata</p>;
        }

        return (
            <div className="container">
                <div className="map">
                    <img src={this.props.groblje.karta} alt={this.props.groblje.naziv} width="800"/>
                </div>
                <div className="text text-center">
                    <input id="ime" placeholder="Ime" ref="ime"/>
                    <input id="prezime" placeholder="Prezime" ref="prezime"/>
                    {searchResult}
                    <button type="submit" className="btn-route"
                            onClick={(e) => this.validate(e)}>Traži
                    </button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {pokojnici: state.groblja.pokojnici}
};

export default connect(mapStateToProps)(Karta);