import React from "react";
import './UvodniZaslon.css';



const UvodniZaslon = (props) => {

const divStyle = {
  backgroundImage: 'url(' + props.slika + ')',
};

    return (
        <div id="hero" style={divStyle}>
            <div className="overlay"/>
            <div className="intro">
                <h1>{props.naziv}</h1>
                <h2>{props.adresa}</h2>
            </div>

        </div>
    );
};

export default UvodniZaslon;
