import React from "react";
import "./Korisnik.css";

const Korisnik = (props) => {

    return (
         <tr>
             <td>{props.korisnik.id}</td>
             <td>{props.korisnik.ime}</td>
            <td>{props.korisnik.prezime}</td>
             <td>{props.korisnik.oib}</td>
             <td>{props.korisnik.korisnickoIme}</td>
            <td>{props.korisnik.uloga}</td>
        </tr>
    );
};

export default Korisnik;