import React, {Component} from 'react';
import './Ljubimac.css';
import Modal from "react-bootstrap/es/Modal";
import defaultLjubimacSlika from './../../static/paw.png'
import {formatDate} from "../../utils/helpFunctions";

class Ljubimac extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    render() {

        //opcionalni parametri
        let ljubimacSlika = this.props.ljubimac.slika == null || this.props.ljubimac.slika === "" ?
            defaultLjubimacSlika : this.props.ljubimac.slika;

        let datumRodenja = this.props.ljubimac.datumRodenja == null ?
            <div/> :
            <li>
                <div className="ljubimac-label">Datum rođenja</div>
                <div className="value">{formatDate(this.props.ljubimac.datumRodenja)}</div>
            </li>;

        let datumSmrti = this.props.ljubimac.datumSmrti == null ?
            <div/> :
            <li>
                <div className="ljubimac-label">Datum smrti</div>
                <div className="value">{formatDate(this.props.ljubimac.datumSmrti)}</div>
            </li>;

        let opis = this.props.ljubimac.opis == null || this.props.ljubimac.opis === "" ?
            <div/> :
            <li>
                <div className="ljubimac-label">Opis</div>
                <div className="value opis"><i>"{this.props.ljubimac.opis}"</i></div>
            </li>;

        return (
            <div className="ljubimac col-sm-4 col-xs-6 flex-col">

                <div className="ljubimac-dio" onClick={this.openModal}>
                    <div className="lijevo">
                        <div className="slika-wrapper">
                            <img src={ljubimacSlika} alt={this.props.ljubimac.ime}/>
                        </div>
                    </div>


                    <ul className="labels">
                        <li>
                            <div className="ljubimac-label">Ime</div>
                            <div className="value">{this.props.ljubimac.ime}</div>
                        </li>
                        <li>
                            <div className="ljubimac-label">Vrsta</div>
                            <div className="value">{this.props.ljubimac.vrsta}</div>
                        </li>
                        {datumRodenja}
                        {datumSmrti}
                    </ul>
                </div>

                <Modal
                    show={this.state.modalIsOpen}
                    onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Informacije o kućnom ljubimcu
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="ljubimac modalWrap">
                            <div className="modalLeft">
                                <div className="slika-wrapper">
                                    <img src={ljubimacSlika} alt={this.props.ljubimac.ime}/>
                                </div>
                            </div>
                            <div className="modalRight">
                                <ul className="labels">
                                    <li>
                                        <div className="ljubimac-label">Ime</div>
                                        <div className="value">{this.props.ljubimac.ime}</div>
                                    </li>
                                    <li>
                                        <div className="ljubimac-label">Vrsta</div>
                                        <div className="value">{this.props.ljubimac.vrsta}</div>
                                    </li>
                                    {datumRodenja}
                                    {datumSmrti}
                                    {opis}
                                    <li>
                                        <div className="ljubimac-label">Vlasnik</div>
                                        <div className="value">{this.props.ljubimac.vlasnikImePrezime}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        )
    };
}


export default Ljubimac;
