import React from 'react';

const ObavijestItem = (props) => {

    return (
        <div className="col-sm-4 animate-box">
            <a className="card">
                <div className="att-image">
                    <img src={props.obavijest.slika} alt="slika"/>
                </div>
                <div className="card-body">
                    <h3>{props.obavijest.naslov}</h3>
                    <p>{props.obavijest.tekst}</p>
                </div>
            </a>
        </div>
    )
};

export default ObavijestItem;
