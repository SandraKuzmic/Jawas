import React, {Component} from "react";
import {connect} from "react-redux";
import grobljeActionCreator from "../actionCreators/grobljeActionCreator";
import ljubimacActionCreator from "../actionCreators/ljubimacActionCreator";

class DataInit extends Component {

    componentDidMount() {
        this.props.dispatch(grobljeActionCreator.fetchGroblja());
        this.props.dispatch(ljubimacActionCreator.fetchLjubimce());
    }

    render() {
        return (
            <div/>
        )
    }

}

export default connect()(DataInit);