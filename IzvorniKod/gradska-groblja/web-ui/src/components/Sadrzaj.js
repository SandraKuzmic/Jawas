import React from "react";
import "./Sadrzaj.css";

const Sadrzaj = (props) => {
    return (
        <div>
            <li>{props.sadrzaj.vrsta},
                {props.sadrzaj.lokacija}</li>
            <br/>
        </div>
    );
};

export default Sadrzaj;