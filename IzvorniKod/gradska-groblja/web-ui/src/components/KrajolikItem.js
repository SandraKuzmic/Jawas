import React from "react";
import "./Groblje.css";
import paths from "../constants/paths";
import {Link} from 'react-router-dom';

const KrajolikItem = (props) => {

    let id = props.groblje.id;

    return (
        <li>
            <label><Link to={paths.routes.krajolik(id)}>{props.groblje.naziv}</Link></label>
            <label>{props.groblje.adresa}</label>
            <label>{props.groblje.telefon}</label>
        </li>
    );
};

export default KrajolikItem;
