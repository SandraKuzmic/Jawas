import React, {Component} from "react";
import Modal from "react-bootstrap/es/Modal";
import defaultSlikaKrajolik from './../../static/krajolik_default.png';
import defaultSlikaLicnost from './../../static/licnost_default.jpg';
import './ElementMuzeja.css';

class ElementMuzeja extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false
        };

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    render() {

        let defaultSlika = this.props.isLicnost ? defaultSlikaLicnost : defaultSlikaKrajolik;

        let slika = this.props.element.slika == null ?
            defaultSlika : this.props.element.slika;

        let naziv = this.props.isLicnost ? this.props.element.ime + " " + this.props.element.prezime
            : this.props.element.naziv;

        let lokacija = this.props.isLicnost ? this.props.element.grobnoMjestoString : this.props.element.lokacija;

        return (
            <div className="item">
                <ul onClick={this.openModal}>
                    <li>
                        <div className="image-wrapper">
                            <img src={slika} alt={naziv}/>
                        </div>
                    </li>
                    <li><h4>{naziv}</h4></li>
                    <li>{lokacija}</li>
                </ul>

                <div className="modal">
                    <Modal
                        show={this.state.modalIsOpen}
                        onHide={this.closeModal}>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                {this.props.isLicnost ? "Informacije o ličnosti" : "Informacije o krajoliku"}
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="modalWrap">
                                <ul className="modal-list">
                                    <div className="modalLeft">
                                        <li>
                                            <div className="image-wrapper">
                                                <img src={slika} alt={naziv}/>
                                            </div>
                                        </li>
                                    </div>
                                    <div className="modalRight">
                                        <li><h3>{naziv}</h3></li>
                                        <li>{this.props.element.opis}</li>
                                        <li>{lokacija}</li>
                                        <li>{this.props.isLicnost? this.props.element.grobnoMjesto.groblje.naziv : ""}</li>
                                    </div>
                                </ul>
                            </div>
                        </Modal.Body>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default ElementMuzeja;
