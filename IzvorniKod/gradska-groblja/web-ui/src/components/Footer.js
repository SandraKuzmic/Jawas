import React, {Component} from 'react';
import './Footer.css';
import paths from '../constants/paths';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 footer-box one">
                            <h3 className="footer-heading">Gradska groblja</h3>
                            <ul className="footer-links">
                                <li><Link to={paths.routes.grobljeList}>Groblja</Link></li>
                                <li><Link to={paths.routes.krajolikList}>Muzej</Link></li>
                                <li><Link to={paths.routes.ljubimacList}>Ljubimci</Link></li>
                                <li>
                                    {this.props.loggedIn ?
                                        <Link to={paths.routes.odjava}>Odjava</Link>
                                        : <Link to={paths.routes.prijava}>Prijava</Link>}
                                </li>
                                {!this.props.loggedIn ?
                                    <li><Link to={paths.routes.registracija}>Registracija</Link></li>
                                    : ""}
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        );
    };
}

const mapStateToProps = (state) => {
    return {loggedIn: !!state.session.token};
};

export default connect(mapStateToProps)(Footer);
