import React from "react";
import "./Usluga.css";

const Usluga = (props) => {
    return (
        <div>
            <li>{props.usluga.naziv},
                {props.usluga.opis}
            </li>
            <br/>
        </div>
    );
};

export default Usluga;