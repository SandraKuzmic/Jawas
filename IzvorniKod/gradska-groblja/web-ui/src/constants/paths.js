let BACKEND = "";

const paths = {
    restApi: {
        ljubimci: BACKEND + "/api/ljubimac",
        ljubimciDodaj: "/api/ljubimac/add",
        fetchLjubimac: (id) => {
            return BACKEND + "/api/ljubimac/" + id
        },
        groblja: BACKEND + "/api/groblje",
        fetchGroblje: (id) => {
            return BACKEND + "/api/groblje/" + id
        },
        fetchPokojnikeNaGroblju: (id) => {
            return BACKEND + "/api/groblje/" + id + "/pokojnik"
        },
        obavijesti: (id) => {
            return BACKEND + "/api/groblje/" + id + "/obavijest"
        },
        sadrzaji: BACKEND + "/api/sadrzaj",
        fetchSadrzaj: (id) => {
            return BACKEND + "/api/sadrzaj/" + id
        },
        korisnici: BACKEND + "/api/korisnik",
        fetchKorisnik: (id) => {
            return BACKEND + "/api/korisnik/" + id
        },
        usluge: BACKEND + "/api/usluga",
        fetchUsluga: (id) => {
            return BACKEND + "/api/usluga/" + id
        },
        sprovodi: BACKEND + "/api/sprovod",
        fetchSprovod: (id) => {
            return BACKEND + "/api/sprovod/" + id
        },
        pokojnici: BACKEND + "/api/pokojnik",
        fetchPokojnike: (id) => {
            return BACKEND + "/api/pokojnik/" + id
        },
        korisniciUslugaGrobnoMjesto: BACKEND + "/api/korisnik_usluga_mjesto",
        fetchKorisniciUslugaGrobnoMjesto: (id) => {
            return BACKEND + "/api/korisnik_usluga_mjesto/" + id
        },
        grobnaMjesta: BACKEND + "/api/grobnoMjesto",
        fetchGrobnaMjesta: (id) => {
            return BACKEND + "/api/grobnoMjesto/" + id
        },
        krajolici: BACKEND + "/api/krajolik",
        fetchKrajolik: (id) => {
            return BACKEND + "/api/krajolik/" + id
        },
        licnosti: BACKEND + "/api/licnost",
        fetchLicnost: (id) => {
            return BACKEND + "/api/licnost/" + id
        },
        registracija: BACKEND + "/api/registracija",
        prijava: BACKEND + "/api/prijava"
    },

    routes: {
        pocetna: "/",
        ljubimacList: "/ljubimci",
        ljubimac: (id) => {
            return "/ljubimci/" + id
        },
        grobljeList: "/groblja",
        groblje: (id) => {
            return "/groblja/" + id
        },
        sadrzajList: "/sadrzaji/",
        sadrzaj: (id) => {
            return "/sadrzaji/" + id
        },
        prijava: "/prijava",
        odjava: "/odjava",
        registracija: "/registracija",
        admin: "/admin",
        usluga: "/usluge",
        sprovod: "/sprovod",
        pokojnik: "/pokojnik",
        korisnikUslugaGrobnoMjesto: "/korisnikUslugaGrobnoMjesto",
        grobnoMjesto: "/grobnoMjesto",
        krajolikList: "/muzej/",
        krajolik: (id) => {
            return "/muzej/" + id
        },
        licnostList: "/licnosti",
        licnost: (id) => {
            return "/licnosti/" + id
        }
    },

};

export default paths;