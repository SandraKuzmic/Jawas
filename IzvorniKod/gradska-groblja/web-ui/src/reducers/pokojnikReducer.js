import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const pokojnikReducer = (state = initialState.pokojnici, action) => {
    switch (action.type) {
        case types.FETCH_POKOJNIKE:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_POKOJNIKA:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default pokojnikReducer;