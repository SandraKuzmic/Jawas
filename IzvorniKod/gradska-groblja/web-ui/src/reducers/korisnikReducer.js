import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const korisnikReducer = (state = initialState.korisnici, action) => {
    switch (action.type) {
        case types.FETCH_KORISNICI:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_KORISNIK:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default korisnikReducer;