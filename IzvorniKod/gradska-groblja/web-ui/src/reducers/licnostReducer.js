import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const licnostReducer = (state = initialState.licnosti, action) => {
    switch (action.type) {
        case types.FETCH_LICNOSTI:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_LICNOST:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default licnostReducer;