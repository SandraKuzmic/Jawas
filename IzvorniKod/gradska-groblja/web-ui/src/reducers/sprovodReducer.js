import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const sprovodReducer = (state = initialState.sprovodi, action) => {
    switch (action.type) {
        case types.FETCH_SPROVODE:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_SPROVOD:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default sprovodReducer;