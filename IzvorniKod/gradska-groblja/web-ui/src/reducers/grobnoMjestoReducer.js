import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const grobnoMjestoReducer = (state = initialState.grobnaMjesta, action) => {
    switch (action.type) {
        case types.FETCH_GROBNA_MJESTA:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_GROBNO_MJESTO:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default grobnoMjestoReducer;