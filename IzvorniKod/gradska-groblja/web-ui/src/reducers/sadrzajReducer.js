import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const sadrzajReducer = (state = initialState.sadrzaji, action) => {
    switch (action.type) {
        case types.FETCH_SADRZAJE:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_SADRZAJ:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

    export default sadrzajReducer;