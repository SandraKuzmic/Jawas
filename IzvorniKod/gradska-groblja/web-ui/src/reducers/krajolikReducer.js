import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const krajolikReducer = (state = initialState.krajolici, action) => {
    switch (action.type) {
        case types.FETCH_KRAJOLIKE:
            if (action.status === "success") {
                return {
                    ...state,
                    all: action.data
                };
            } else {
                return state;
            }
        case types.FETCH_KRAJOLIK:
            if (action.status === "success") {
                return {
                    ...state,
                    current: action.data
                };
            } else {
                return {
                    ...state,
                    current: null
                };
            }
        default:
            return state;
    }
};

export default krajolikReducer;