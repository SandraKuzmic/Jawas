import * as types from "./actionTypes";

export const fetchSadrzaje = ({status, data}) => {
    return {type: types.FETCH_SADRZAJE, status, data}
};

export const fetchSadrzaj = ({status, data}) => {
    return {type: types.FETCH_SADRZAJ, status, data}
};