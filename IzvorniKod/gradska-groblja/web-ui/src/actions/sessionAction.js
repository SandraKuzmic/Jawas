import * as types from "./actionTypes";

export const postRegistracija = ({error, token, korisnik}) => {
return {type: types.POST_REGISTRACIJA, error, token, korisnik}
};

export const postPrijava = ({error, token, korisnik}) => {
    return {type: types.POST_PRIJAVA, error, token, korisnik}
}

export const noneOdjava = {
    type: types.NONE_ODJAVA
};
