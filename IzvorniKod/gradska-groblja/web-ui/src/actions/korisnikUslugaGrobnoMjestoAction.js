import * as types from "./actionTypes";

export const fetchKorisniciUslugaGrobnoMjesto = ({status, data}) => {
    return {type: types.FETCH_KORISNICI_USLUGA_GROBNO_MJESTO, status, data}
};

export const fetchKorisnikUslugaGrobnoMjesto = ({status, data}) => {
    return {type: types.FETCH_KORISNIK_USLUGA_GROBNO_MJESTO, status, data}
};