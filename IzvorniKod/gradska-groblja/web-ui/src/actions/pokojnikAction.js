import * as types from "./actionTypes";

export const fetchPokojnike = ({status, data}) => {
    return {type: types.FETCH_POKOJNIKE, status, data}
};

export const fetchPokojnika = ({status, data}) => {
    return {type: types.FETCH_POKOJNIKA, status, data}
};