import * as types from "./actionTypes";

export const fetchKorisnici = ({status, data}) => {
    return {type: types.FETCH_KORISNICI, status, data}
};

export const fetchKorisnik = ({status, data}) => {
    return {type: types.FETCH_KORISNIK, status, data}
};