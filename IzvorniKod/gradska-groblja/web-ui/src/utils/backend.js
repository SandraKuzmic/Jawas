let paths = require("../constants/paths");

let superagent = require('superagent');
let Request = superagent.Request;
let originalEnd = Request.prototype.end;

Request.prototype.end = function () {
    this.end = originalEnd;
    this.use((request) => {
        request.on('response', (res) => {
            if (res.status === 401) {
                window.location.href = paths.default.routes.login
            }
        });
    });
    return this.end.apply(this, arguments);
};

module.exports = superagent;