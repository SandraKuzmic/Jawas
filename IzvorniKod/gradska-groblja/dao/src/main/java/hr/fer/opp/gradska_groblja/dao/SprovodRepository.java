package hr.fer.opp.gradska_groblja.dao;

import hr.fer.opp.gradska_groblja.model.Sprovod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SprovodRepository extends JpaRepository<Sprovod, Long> {
}
