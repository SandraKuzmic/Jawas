package hr.fer.opp.gradska_groblja.dao;

import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KucniLjubimacSpomenik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KucniLjubimacSpomenikRepository extends JpaRepository<KucniLjubimacSpomenik, Long> {

    List<KucniLjubimacSpomenik> findByKorisnik(Korisnik korisnik);


}
