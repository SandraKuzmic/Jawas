package hr.fer.opp.gradska_groblja.dao;

import hr.fer.opp.gradska_groblja.model.Obavijest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObavijestRepository extends JpaRepository<Obavijest, Long>{
}
