package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Licnost extends Pokojnik {

    @Column
    private String slika;

    @Column
    private String opis;

    public Licnost() {
    }

    public Licnost(Long id, String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti,
                   LocalDate datumUkopa, GrobnoMjesto grobnoMjesto, String slika, String opis, LocalDateTime kreirano, String korisnikKreirano,
                   LocalDateTime izmjena, String korisnikIzmjena) {
        super(id, ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, kreirano, korisnikKreirano, izmjena, korisnikIzmjena);
        this.slika = slika;
        this.opis = opis;
    }

    public Licnost(String ime, String prezime, LocalDate datumRodenja, LocalDate datumSmrti, LocalDate datumUkopa,
                   GrobnoMjesto grobnoMjesto, String slika, String opis, LocalDateTime kreirano, String korisnikKreirano,
                   LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, slika, opis, kreirano, korisnikKreirano, izmjena, korisnikIzmjena);
    }

    public Licnost(String opis, String slika) {
        this.opis = opis;
        this.slika = slika;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
