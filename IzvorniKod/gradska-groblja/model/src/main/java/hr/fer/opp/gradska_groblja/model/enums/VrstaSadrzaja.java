package hr.fer.opp.gradska_groblja.model.enums;

public enum VrstaSadrzaja {
    GLAVNI_ULAZ, KANTE_ZA_VODU, PUMPA_ZA_VODU, PRODAJA_SVIJECA_I_CVIJECA, KONTEJNER
}
