package hr.fer.opp.gradska_groblja.model.enums;

public enum Privatnost {

    JAVNO, PRIVATNO, SAMO_VLASNIK
}
