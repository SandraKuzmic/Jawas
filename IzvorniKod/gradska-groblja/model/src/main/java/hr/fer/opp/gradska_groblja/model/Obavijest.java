package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Obavijest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String naslov;

    @Column(nullable = false)
    private String tekst;

    @Column
    private String slika;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "groblje_id")
    @JsonIgnore
    private Groblje groblje;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Obavijest() {

    }

    public Obavijest(Long id, String naslov, String tekst, String slika, Groblje groblje, LocalDateTime kreirano,
                     String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.naslov = naslov;
        this.tekst = tekst;
        this.slika = slika;
        this.groblje = groblje;
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Obavijest(String naslov, String tekst, String slika, Groblje groblje, LocalDateTime kreirano,
                     String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, naslov, tekst, slika, groblje, kreirano, korisnikKreirano, izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public Groblje getGroblje() {
        return groblje;
    }

    public void setGroblje(Groblje groblje) {
        this.groblje = groblje;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public void setKreirano(LocalDateTime kreirano) {
        this.kreirano = kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public void setKorisnikKreirano(String korisnikKreirano) {
        this.korisnikKreirano = korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public void setIzmjena(LocalDateTime izmjena) {
        this.izmjena = izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    public void setKorisnikIzmjena(String korisnikIzmjena) {
        this.korisnikIzmjena = korisnikIzmjena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Obavijest obavijest = (Obavijest) o;
        return Objects.equals(id, obavijest.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
