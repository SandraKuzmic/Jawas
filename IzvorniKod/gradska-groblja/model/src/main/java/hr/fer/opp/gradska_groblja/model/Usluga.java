package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Usluga implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String naziv;

    @Column
    private String opis;

    @Column(nullable = false)
    private Double cijena;

    @OneToMany(mappedBy = "usluga")
    @JsonIgnore
    private List<KorisnikUslugaGrobnoMjesto> uslugaPoGrobnomMjestu;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Usluga() {
    }

    public Usluga(Long id, String naziv, String opis, Double cijena, LocalDateTime kreirano, String korisnikKreirano, LocalDateTime izmjena,
                  String korisnikIzmjena) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.cijena = cijena;
        this.uslugaPoGrobnomMjestu = new LinkedList<>();
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Usluga(String naziv, String opis, Double cijena, LocalDateTime kreirano, String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, naziv, opis, cijena, kreirano, korisnikKreirano, izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Double getCijena() {
        return cijena;
    }

    public void setCijena(Double cijena) {
        this.cijena = cijena;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    public List<KorisnikUslugaGrobnoMjesto> getUslugaPoGrobnomMjestu() {
        return uslugaPoGrobnomMjestu;
    }

    public void setUslugaPoGrobnomMjestu(List<KorisnikUslugaGrobnoMjesto> uslugaPoGrobnomMjestu) {
        this.uslugaPoGrobnomMjestu = uslugaPoGrobnomMjestu;
    }

    public void addUsluga(KorisnikUslugaGrobnoMjesto usluga){
        usluga.setUsluga(this);
        this.uslugaPoGrobnomMjestu.add(usluga);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usluga usluga = (Usluga) o;
        return Objects.equals(id, usluga.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
