package hr.fer.opp.gradska_groblja.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hr.fer.opp.gradska_groblja.model.enums.Uloga;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Korisnik implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String oib;

    @Column(nullable = false)
    private String ime;

    @Column(nullable = false)
    private String prezime;

    @Column(nullable = false)
    private String korisnickoIme;

    @Column(nullable = false)
    @JsonIgnore
    private String lozinka;

    @Column(nullable = false)
    @ColumnDefault("'KORISNIK'")
    @Enumerated(EnumType.STRING)
    private Uloga uloga;

    @OneToMany(mappedBy = "korisnik")
    private List<KucniLjubimacSpomenik> listLjubimci;

    @ManyToMany
    @JoinTable(name = "vlasnik_mjesta",
            joinColumns = @JoinColumn(name = "korisnik_id"),
            inverseJoinColumns = @JoinColumn(name = "grobno_mjesto_id"))
    private List<GrobnoMjesto> listGrobnaMjesta;

    @OneToMany(mappedBy = "korisnik")
    @JsonIgnore
    private List<KorisnikUslugaGrobnoMjesto> uslugaPoGrobnomMjestu;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Korisnik() {
    }

    public Korisnik(Long id, String oib, String imeKorisnik, String prezimeKorisnik, String korisnickoIme, String lozinka, Uloga uloga, LocalDateTime kreirano,
                    String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.oib = oib;
        this.ime = imeKorisnik;
        this.prezime = prezimeKorisnik;
        this.korisnickoIme = korisnickoIme;
        this.lozinka = lozinka;
        this.uloga = uloga;
        this.listLjubimci = new LinkedList<>();
        this.listGrobnaMjesta = new LinkedList<>();
        this.uslugaPoGrobnomMjestu = new LinkedList<>();
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Korisnik(String oib, String imeKorisnik, String prezimeKorisnik, String korisnickoIme, String lozinka, Uloga uloga, LocalDateTime kreirano,
                    String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, oib, imeKorisnik, prezimeKorisnik, korisnickoIme, lozinka, uloga, kreirano, korisnikKreirano,
                izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public Uloga getUloga() {
        return uloga;
    }

    public void setUloga(Uloga uloga) {
        this.uloga = uloga;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public List<KucniLjubimacSpomenik> getListLjubimci() {
        return listLjubimci;
    }

    public void setListLjubimci(List<KucniLjubimacSpomenik> listLjubimci) {
        this.listLjubimci = listLjubimci;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    public void addLjubimac(KucniLjubimacSpomenik ljubimac) {
        ljubimac.setKorisnik(this);
        listLjubimci.add(ljubimac);
    }

    public List<GrobnoMjesto> getListGrobnaMjesta() {
        return listGrobnaMjesta;
    }

    public void setListGrobnaMjesta(List<GrobnoMjesto> listGrobnaMjesta) {
        this.listGrobnaMjesta = listGrobnaMjesta;
    }

    public void addGrobnoMjesto(GrobnoMjesto grobnoMjesto) {
        grobnoMjesto.addVlasnik(this);
        this.listGrobnaMjesta.add(grobnoMjesto);
    }

    public List<KorisnikUslugaGrobnoMjesto> getUslugaPoGrobnomMjestu() {
        return uslugaPoGrobnomMjestu;
    }

    public void setUslugaPoGrobnomMjestu(List<KorisnikUslugaGrobnoMjesto> uslugaPoGrobnomMjestu) {
        this.uslugaPoGrobnomMjestu = uslugaPoGrobnomMjestu;
    }

    public void addUsluga(KorisnikUslugaGrobnoMjesto usluga) {
        usluga.setKorisnik(this);
        this.uslugaPoGrobnomMjestu.add(usluga);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Korisnik)) return false;

        Korisnik korisnik = (Korisnik) o;

        return getId() != null ? getId().equals(korisnik.getId()) : korisnik.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
