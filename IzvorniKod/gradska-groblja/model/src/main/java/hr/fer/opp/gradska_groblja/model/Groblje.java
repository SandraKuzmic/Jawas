package hr.fer.opp.gradska_groblja.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Groblje implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String naziv;

    @Column(nullable = false)
    private String adresa;

    @Column(nullable = false)
    private String telefon;

    @Column
    private String url;

    @Column
    private String email;

    @Column(nullable = false)
    private LocalTime radniDanOd;

    @Column(nullable = false)
    private LocalTime radniDanDo;

    @Column(nullable = false)
    private LocalTime vikendOd;

    @Column(nullable = false)
    private LocalTime vikendDo;

    @Column(nullable = false)
    private LocalTime prijemPokojnikaOd;

    @Column(nullable = false)
    private LocalTime prijemPokojnikaDo;

    @Column
    private String slika;

    @Column
    private String karta;

    @OneToMany(mappedBy = "groblje")
    private List<Krajolik> listKrajolika;

    @OneToMany(mappedBy = "groblje")
    @JsonIgnore
    private List<GrobnoMjesto> listGrobnaMjesta;

    @OneToMany(mappedBy = "groblje")
    private List<Sadrzaj> listSadrzaja;

    @OneToMany(mappedBy = "groblje")
    private List<Obavijest> listObavijesti;

    @Column
    @JsonIgnore
    private LocalDateTime kreirano;

    @Column
    @JsonIgnore
    private String korisnikKreirano;

    @Column
    @JsonIgnore
    private LocalDateTime izmjena;

    @Column
    @JsonIgnore
    private String korisnikIzmjena;

    public Groblje() {
    }

    public Groblje(Long id, String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo,
                   LocalTime vikendOd, LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo, String slika, String karta,
                   LocalDateTime kreirano, String korisnikKreirano, LocalDateTime izmjena, String korisnikIzmjena) {
        this.id = id;
        this.naziv = naziv;
        this.adresa = adresa;
        this.telefon = telefon;
        this.url = url;
        this.email = email;
        this.radniDanOd = radniDanOd;
        this.radniDanDo = radniDanDo;
        this.vikendOd = vikendOd;
        this.vikendDo = vikendDo;
        this.slika = slika;
        this.karta = karta;
        this.prijemPokojnikaOd = prijemPokojnikaOd;
        this.prijemPokojnikaDo = prijemPokojnikaDo;
        this.listKrajolika = new ArrayList<>();
        this.listGrobnaMjesta = new ArrayList<>();
        this.listSadrzaja = new ArrayList<>();
        this.listObavijesti = new ArrayList<>();
        this.kreirano = kreirano;
        this.korisnikKreirano = korisnikKreirano;
        this.izmjena = izmjena;
        this.korisnikIzmjena = korisnikIzmjena;
    }

    public Groblje(String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo, LocalTime vikendOd,
                   LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo, String slika, String karta,
                   LocalDateTime kreirano, String korisnikKreirano,
                   LocalDateTime izmjena, String korisnikIzmjena) {
        this(null, naziv, adresa, telefon, url, email, radniDanOd, radniDanDo, vikendOd, vikendDo, prijemPokojnikaOd, prijemPokojnikaDo,
                slika, karta, kreirano, korisnikKreirano, izmjena, korisnikIzmjena);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalTime getRadniDanOd() {
        return radniDanOd;
    }

    public void setRadniDanOd(LocalTime radniDanOd) {
        this.radniDanOd = radniDanOd;
    }

    public LocalTime getRadniDanDo() {
        return radniDanDo;
    }

    public void setRadniDanDo(LocalTime radniDanDo) {
        this.radniDanDo = radniDanDo;
    }

    public LocalTime getVikendOd() {
        return vikendOd;
    }

    public void setVikendOd(LocalTime vikendOd) {
        this.vikendOd = vikendOd;
    }

    public LocalTime getVikendDo() {
        return vikendDo;
    }

    public void setVikendDo(LocalTime vikendDo) {
        this.vikendDo = vikendDo;
    }

    public LocalTime getPrijemPokojnikaOd() {
        return prijemPokojnikaOd;
    }

    public void setPrijemPokojnikaOd(LocalTime prijemPokojnikaOd) {
        this.prijemPokojnikaOd = prijemPokojnikaOd;
    }

    public LocalTime getPrijemPokojnikaDo() {
        return prijemPokojnikaDo;
    }

    public void setPrijemPokojnikaDo(LocalTime prijemPokojnikaDo) {
        this.prijemPokojnikaDo = prijemPokojnikaDo;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public String getKarta() {
        return karta;
    }

    public void setKarta(String karta) {
        this.karta = karta;
    }

    public List<Krajolik> getListKrajolika() {
        return listKrajolika;
    }

    public void setListKrajolika(List<Krajolik> listKrajolika) {
        this.listKrajolika = listKrajolika;
    }

    public LocalDateTime getKreirano() {
        return kreirano;
    }

    public String getKorisnikKreirano() {
        return korisnikKreirano;
    }

    public LocalDateTime getIzmjena() {
        return izmjena;
    }

    public String getKorisnikIzmjena() {
        return korisnikIzmjena;
    }

    public void addKrajolik(Krajolik krajolik) {
        krajolik.setGroblje(this);
        listKrajolika.add(krajolik);
    }

    public List<GrobnoMjesto> getListGrobnaMjesta() {
        return listGrobnaMjesta;
    }

    public void setListGrobnaMjesta(List<GrobnoMjesto> listGrobnaMjesta) {
        this.listGrobnaMjesta = listGrobnaMjesta;
    }

    public void addGrobnoMjesto(GrobnoMjesto grobnoMjesto) {
        grobnoMjesto.setGroblje(this);
        listGrobnaMjesta.add(grobnoMjesto);
    }

    public List<Sadrzaj> getListSadrzaja() {
        return listSadrzaja;
    }

    public void setListSadrzaja(List<Sadrzaj> listSadrzaja) {
        this.listSadrzaja = listSadrzaja;
    }

    public void addSadrzaj(Sadrzaj sadrzaj) {
        sadrzaj.setGroblje(this);
        listSadrzaja.add(sadrzaj);
    }

    public List<Obavijest> getListObavijesti() {
        return listObavijesti;
    }

    public void setListObavijesti(List<Obavijest> listObavijesti) {
        this.listObavijesti = listObavijesti;
    }

    public void addObavijest(Obavijest obavijest) {
        obavijest.setGroblje(this);
        listObavijesti.add(obavijest);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Groblje that = (Groblje) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


}
