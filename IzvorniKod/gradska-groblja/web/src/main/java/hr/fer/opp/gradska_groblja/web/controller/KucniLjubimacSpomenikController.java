package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KucniLjubimacSpomenik;
import hr.fer.opp.gradska_groblja.model.enums.Privatnost;
import hr.fer.opp.gradska_groblja.service.IKorisnikService;
import hr.fer.opp.gradska_groblja.service.IKucniLjubimacSpomenikService;
import hr.fer.opp.gradska_groblja.web.form.LjubimacForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/ljubimac")
public class KucniLjubimacSpomenikController {
    private final IKucniLjubimacSpomenikService ljubimacService;
    private final IKorisnikService korisnikService;

    @Autowired
    public KucniLjubimacSpomenikController(IKucniLjubimacSpomenikService ljubimacService, IKorisnikService korisnikService) {
        this.ljubimacService = ljubimacService;
        this.korisnikService = korisnikService;
    }

    //u postmanu testirate da u body odaberete json
    @PostMapping("/add")
    public ResponseEntity<?> add(@RequestBody LjubimacForm form, BindingResult bindingResult, HttpServletRequest request) {
        String korisnickoIme = (String) request.getAttribute("korisnickoIme");

        Optional<Korisnik> korisnikOptional = korisnikService.findByKorisnickoIme(korisnickoIme);

        if (!korisnikOptional.isPresent()) {
            return ResponseEntity.badRequest().body("Non existing user");
        }

        Korisnik korisnik = korisnikOptional.get();

        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body("Error on binding");
        }

        String ime = form.getIme();
        String vrsta = form.getVrsta();
        Privatnost privatnost = form.getPrivatnost();
        LocalDate datumRodenja = form.getDatumRodenja();
        LocalDate datumSmrti = form.getDatumSmrti();
        String opis = form.getOpis();
        String slika = form.getSlika();

        if (ime == null || ime.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Ime can't be null or empty");
        }
        if (vrsta == null || vrsta.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrsta can't be null or empty");
        }
        if (privatnost == null) {
            privatnost = Privatnost.JAVNO;
        }

        try {
            KucniLjubimacSpomenik spomenik =
                    ljubimacService.add(ime.trim(), vrsta.trim(), datumRodenja,
                            datumSmrti, opis, slika, privatnost, korisnik, korisnickoIme);
            return ResponseEntity.ok(spomenik);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody KucniLjubimacSpomenik body, HttpServletRequest request) {
        String korisnickoIme = (String) request.getAttribute("korisnickoIme");

        if (body.getIme() == null || body.getIme().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Ime can't be null or empty");
        }
        if (body.getVrsta() == null || body.getVrsta().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Vrsta can't be null or empty");
        }
        try {
            KucniLjubimacSpomenik spomenik = ljubimacService.save(id, body.getIme().trim(), body.getVrsta().trim(), body.getDatumRodenja(),
                    body.getDatumSmrti(), body.getOpis(), body.getSlika(), body.getPrivatnost() == null ? Privatnost.JAVNO : body.getPrivatnost(),
                    body.getKorisnik(), korisnickoIme);
            return ResponseEntity.ok(spomenik);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public KucniLjubimacSpomenik get(@PathVariable Long id) {
        return ljubimacService.get(id);
    }

    @GetMapping
    public List<KucniLjubimacSpomenik> findAll() {
        return ljubimacService.findAll();
    }

    /**
     * @param date must be in format yyyy-MM-dd
     * @return LocalDate object
     * @throws IllegalArgumentException if date format is incorrect
     */
    public LocalDate getLocalDateFromString(String date) {
        Pattern pattern = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})");
        Matcher matcher = pattern.matcher(date);
        if (matcher.matches()) {
            return LocalDate.of(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3)));
        } else {
            throw new IllegalArgumentException("Date format incorrect");
        }
    }
}
