package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.enums.Uloga;
import hr.fer.opp.gradska_groblja.service.IKorisnikService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class SessionController {
    private final IKorisnikService korisnikService;
    private final int passwordMinLength;
    private final int oibLength;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public static final int EXPIRATION_TIME = (60 * 60 * 24 * 7 * 1000);
    public static final String SECRET = "1234567890abcd";

    @Autowired
    public SessionController(IKorisnikService korisnikService, @Value("${limits.password.min_length}") int passwordMinLength,
                             @Value("${limits.oib.length}") int oibLength, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.korisnikService = korisnikService;
        this.passwordMinLength = passwordMinLength;
        this.oibLength = oibLength;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    private HashMap<String, Object> returnUserToken(Korisnik korisnik) {
        final HashMap<String, Object> response = new HashMap<>();
        final String token = Jwts.builder()
                .setSubject(korisnik.getKorisnickoIme())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
                .compact();

        response.put("token", token);
        response.put("korisnik", korisnik);
        return response;
    }

    @PostMapping("/registracija")
    public ResponseEntity<?> register(@RequestBody Map<String, String> json) {
        String oib = json.get("oib");
        String ime = json.get("ime");
        String prezime = json.get("prezime");
        String korisnickoIme = json.get("korisnickoIme");
        String lozinka = json.get("lozinka");

        if (oib == null || oib.trim().length() != oibLength)
            return ResponseEntity.badRequest().body("OIB nije važeće duljine");

        if (ime == null || ime.trim().isEmpty())
            return ResponseEntity.badRequest().body("Ime ne može biti prazno");

        if (prezime == null || prezime.trim().isEmpty())
            return ResponseEntity.badRequest().body("Prezime ne može biti prazno");

        if (korisnickoIme == null || korisnickoIme.trim().isEmpty())
            return ResponseEntity.badRequest().body("Korisnicko ime ne može biti prazno");

        if (korisnikService.findByKorisnickoIme(korisnickoIme).isPresent())
            return ResponseEntity.badRequest().body("Već postoji korisnik sa tim korisničkim imenom");

        if (lozinka == null || lozinka.trim().isEmpty())
            return ResponseEntity.badRequest().body("Lozinka ne može biti prazna");

        if (lozinka.trim().length() < passwordMinLength)
            return ResponseEntity.badRequest().body("Lozinka mora imati barem " + passwordMinLength + " znakova");

        String sifriranaLozinka = bCryptPasswordEncoder.encode(lozinka.trim());

        try {
            Korisnik korisnik = korisnikService.add(oib.trim(), ime.trim(), prezime.trim(), korisnickoIme.trim(), sifriranaLozinka, Uloga.KORISNIK, "-");
            return ResponseEntity.ok(returnUserToken(korisnik));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/prijava")
    public ResponseEntity<?> login(@RequestBody Map<String, String> json) {
        String korisnickoIme = json.get("korisnickoIme");
        String lozinka = json.get("lozinka");

        if (korisnickoIme == null || korisnickoIme.trim().isEmpty())
            return ResponseEntity.badRequest().body("Korisničko ime ne može biti prazno");

        if (lozinka == null || lozinka.trim().isEmpty())
            return ResponseEntity.badRequest().body("Lozinka ne može biti prazna");

        Optional<Korisnik> korisnik = korisnikService.findByKorisnickoIme(korisnickoIme);

        if (korisnik.isPresent() &&
                (lozinka.equals(korisnik.get().getLozinka()) || bCryptPasswordEncoder.matches(lozinka, korisnik.get().getLozinka()))) {
            return ResponseEntity.ok(returnUserToken(korisnik.get()));
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Nevažeće korisničko ime ili lozinka.");
        }
    }
}
