package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Pokojnik;
import hr.fer.opp.gradska_groblja.service.IGrobnoMjestoService;
import hr.fer.opp.gradska_groblja.service.IPokojnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/pokojnik")
public class PokojnikController {
    private final IPokojnikService pokojnikService;
    private final IGrobnoMjestoService grobnoMjestoService;

    @Autowired
    public PokojnikController(IPokojnikService pokojnikService, IGrobnoMjestoService grobnoMjestoService) {
        this.pokojnikService = pokojnikService;
        this.grobnoMjestoService = grobnoMjestoService;
    }

    //u postmanu testirate da u body odaberete form-data
    @PostMapping
    public ResponseEntity<?> add(String ime, String prezime, String datumRodenja, String datumSmrti, String datumUkopa,
                                 Long grobnoMjestoId) {
        if (ime == null || ime.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Ime can't be null or empty");
        }
        if (prezime == null || prezime.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Prezime can't be null or empty");
        }
        if (datumRodenja != null && datumSmrti != null && datumUkopa != null) {
            if (getLocalDateFromString(datumRodenja).compareTo(getLocalDateFromString(datumSmrti)) >= 0 ||
                    getLocalDateFromString(datumRodenja).compareTo(getLocalDateFromString(datumUkopa)) >= 0 ||
                    getLocalDateFromString(datumSmrti).compareTo(getLocalDateFromString(datumUkopa)) >= 0) {
                return ResponseEntity.badRequest().body("Date chronology is not correct");
            }
        }

        if (grobnoMjestoId == null) {
            return ResponseEntity.badRequest().body("GrobnoMjestoId can't be null or empty");
        }
        GrobnoMjesto grobnoMjesto = grobnoMjestoService.get(grobnoMjestoId);
        if (grobnoMjesto == null) {
            return ResponseEntity.badRequest().body("Can't find grobno mjesto with given id");
        }
        try {
            Pokojnik pokojnik = pokojnikService.add(ime.trim(), prezime.trim(), getLocalDateFromString(datumRodenja), getLocalDateFromString(datumSmrti),
                    getLocalDateFromString(datumUkopa), grobnoMjesto, "lkristof");
            return ResponseEntity.ok(pokojnik);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Pokojnik body) {
        if (body.getIme() == null || body.getIme().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Ime can't be null or empty");
        }
        if (body.getPrezime() == null || body.getPrezime().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Prezime can't be null or empty");
        }
        if (body.getDatumRodenja() != null && body.getDatumSmrti() != null && body.getDatumUkopa() != null) {
            if (body.getDatumRodenja().compareTo(body.getDatumSmrti()) >= 0 ||
                    body.getDatumRodenja().compareTo(body.getDatumUkopa()) >= 0 ||
                    body.getDatumSmrti().compareTo(body.getDatumUkopa()) >= 0) {
                return ResponseEntity.badRequest().body("Date chronology is not correct");
            }
        }
        try {
            Pokojnik pokojnik = pokojnikService.save(id, body.getIme().trim(), body.getPrezime().trim(), body.getDatumRodenja(),
                    body.getDatumSmrti(), body.getDatumUkopa(), body.getGrobnoMjesto(), "lkristof");
            return ResponseEntity.ok(pokojnik);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Pokojnik get(@PathVariable Long id) {
        return pokojnikService.get(id);
    }

    @GetMapping
    public List<Pokojnik> findAll() {
        return pokojnikService.findAll();
    }

    /**
     * @param date must be in format yyyy-MM-dd
     * @return LocalDate object
     * @throws IllegalArgumentException if date format is incorrect
     */
    public LocalDate getLocalDateFromString(String date) {
        if (date == null) {
            return null;
        }
        Pattern pattern = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})");
        Matcher matcher = pattern.matcher(date);
        if (matcher.matches()) {
            return LocalDate.of(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3)));
        } else {
            throw new IllegalArgumentException("Date format incorrect");
        }
    }
}
