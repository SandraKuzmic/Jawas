package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Usluga;
import hr.fer.opp.gradska_groblja.service.IUslugaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/usluga")
public class UslugaController {
    private final IUslugaService uslugaService;

    @Autowired
    public UslugaController(IUslugaService uslugaService) {
        this.uslugaService = uslugaService;
    }

    //u postmanu testirate da u body odaberete form-data
    @PostMapping("/add")
    public ResponseEntity<?> add(String naziv, String opis, Double cijena, HttpServletRequest request) {
        String korisnik = (String) request.getAttribute("korisnickoIme");

        if (naziv == null || naziv.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naziv can't be null or empty");
        }
        if (opis == null || opis.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Opis can't be null or empty");
        }
        if (cijena == null) {
            return ResponseEntity.badRequest().body("Cijena can't be null or empty");
        }
        if (cijena < 0) {
            return ResponseEntity.badRequest().body("Cijena can't be negative");
        }
        try {
            Usluga usluga = uslugaService.add(naziv.trim(), opis.trim(), cijena, korisnik);
            return ResponseEntity.ok(usluga);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Usluga body, HttpServletRequest request) {
        String korisnik = (String) request.getAttribute("korisnickoIme");

        if (body.getNaziv() == null || body.getNaziv().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Naziv can't be null or empty");
        }
        if (body.getOpis() == null || body.getOpis().trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Opis can't be null or empty");
        }
        if (body.getCijena() == null) {
            return ResponseEntity.badRequest().body("Cijena can't be null or empty");
        }
        if (body.getCijena() < 0) {
            return ResponseEntity.badRequest().body("Cijena can`t be negative");
        }
        try {
            Usluga usluga = uslugaService.save(id, body.getNaziv().trim(), body.getOpis().trim(), body.getCijena(), korisnik);
            return ResponseEntity.ok(usluga);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Usluga get(@PathVariable Long id) {
        return uslugaService.get(id);
    }

    @GetMapping
    public List<Usluga> findAll() {
        return uslugaService.findAll();
    }

}
