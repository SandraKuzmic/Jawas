package hr.fer.opp.gradska_groblja.web.controller;

import hr.fer.opp.gradska_groblja.model.Pokojnik;
import hr.fer.opp.gradska_groblja.model.Sprovod;
import hr.fer.opp.gradska_groblja.service.IPokojnikService;
import hr.fer.opp.gradska_groblja.service.ISprovodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/sprovod")
public class SprovodController {
    private final ISprovodService sprovodService;
    private final IPokojnikService pokojnikService;

    @Autowired
    public SprovodController(ISprovodService sprovodService, IPokojnikService pokojnikService) {
        this.sprovodService = sprovodService;
        this.pokojnikService = pokojnikService;
    }

    //u postmanu testirate da u body odaberete form-data
    @PostMapping
    public ResponseEntity<?> add(String pocetak, Long pokojnikId) {
        if (pocetak == null || pocetak.trim().isEmpty()) {
            return ResponseEntity.badRequest().body("Pocetak can't be null or empty");
        }

        if (pokojnikId == null) {
            return ResponseEntity.badRequest().body("PokojnikId can't be null or empty");
        }
        Pokojnik pokojnik = pokojnikService.get(pokojnikId);
        if (pokojnik == null) {
            return ResponseEntity.badRequest().body("Can't find pokojnik with given id");
        }
        try {
            Sprovod sprovod = sprovodService.add(getLocalDateAndTimeFromString(pocetak), pokojnik, "lkristof");
            return ResponseEntity.ok(sprovod);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    //u postmanu testirate da u body stavite raw - json
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Sprovod body) {
        if (body.getPocetak() == null) {
            return ResponseEntity.badRequest().body("Pocetak can't be null or empty");
        }

        try {
            Sprovod sprovod = sprovodService.save(id, body.getPocetak(), body.getPokojnik(), "lkristof");
            return ResponseEntity.ok(sprovod);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Sprovod get(@PathVariable Long id) {
        return sprovodService.get(id);
    }

    @GetMapping
    public List<Sprovod> findAll() {
        return sprovodService.findAll();
    }

    /**
     * @param dateTime must be in format yyyy-MM-dd-HH-mm
     * @return LocalDateTime object
     * @throws IllegalArgumentException if date format is incorrect
     */
    public LocalDateTime getLocalDateAndTimeFromString(String dateTime) {
        Pattern pattern = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})-(\\d{2})-(\\d{2})");
        Matcher matcher = pattern.matcher(dateTime);
        if (matcher.matches()) {
            if (Integer.parseInt(matcher.group(4)) > 23) {
                throw new IllegalArgumentException("Hour value must be in range from 00 to 23");
            } else if (Integer.parseInt(matcher.group(5)) > 59) {
                throw new IllegalArgumentException("Minute value must be in range from 00 to 59");
            }
            return LocalDateTime.of(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3)),
                    Integer.parseInt(matcher.group(4)), Integer.parseInt(matcher.group(5)));
        } else {
            throw new IllegalArgumentException("Date format incorrect");
        }
    }
}
