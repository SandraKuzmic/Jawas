package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.GrobnoMjestoRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.service.IGrobnoMjestoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GrobnoMjestoService implements IGrobnoMjestoService {
    private final GrobnoMjestoRepository grobnoMjestoRepository;

    @Autowired
    public GrobnoMjestoService(GrobnoMjestoRepository grobnoMjestoRepository) {
        this.grobnoMjestoRepository = grobnoMjestoRepository;
    }


    @Override
    public GrobnoMjesto add(String odjel, Short polje, Short razred, Integer broj, Groblje groblje, String korisnikKreiranoIzmjena) {

        this.checkPrerequisites(odjel, polje, razred, broj);
        return grobnoMjestoRepository.save(new GrobnoMjesto(odjel, polje, razred, broj, groblje, LocalDateTime.now(), korisnikKreiranoIzmjena, LocalDateTime.now(),
                korisnikKreiranoIzmjena));
    }

    @Override
    public GrobnoMjesto save(Long id, String odjel, Short polje, Short razred, Integer broj, Groblje groblje, String korisnikIzmjena) {

        this.checkPrerequisites(odjel, polje, razred, broj);
        LocalDateTime kreirano = grobnoMjestoRepository.getOne(id).getKreirano();
        String korisnikKreirano = grobnoMjestoRepository.getOne(id).getKorisnikKreirano();
        return grobnoMjestoRepository.save(new GrobnoMjesto(id, odjel, polje, razred, broj, groblje, kreirano, korisnikKreirano,
                LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public GrobnoMjesto get(Long id) {
        return grobnoMjestoRepository.getOne(id);
    }

    @Override
    public List<GrobnoMjesto> findAll() {
        return grobnoMjestoRepository.findAll();
    }

    private void checkPrerequisites(String odjel, Short polje, Short razred, Integer broj) {
        if (odjel == null || odjel.trim().isEmpty()) {
            throw new IllegalArgumentException("Odjel can't be null or empty");
        }
        if (polje == null) {
            throw new IllegalArgumentException("Polje can't be null or empty");
        }
        if (razred == null) {
            throw new IllegalArgumentException("Razred can't be null or empty");
        }
        if (broj == null) {
            throw new IllegalArgumentException("Broj can't be null or empty");
        }
        if (polje<0 || razred<0 || broj<0) {
            throw new IllegalArgumentException("Polje, razred and broj can`t be negative values");
        }
    }
}
