package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;

import java.util.List;

public interface IGrobnoMjestoService {

    GrobnoMjesto add(String odjel, Short polje, Short razred, Integer broj, Groblje groblje, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    GrobnoMjesto save(Long id, String odjel, Short polje, Short razred, Integer broj, Groblje groblje,
                      String korisnikKreiranoIzmjena) throws IllegalAccessException;

    GrobnoMjesto get(Long id);

    List<GrobnoMjesto> findAll();
}
