package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Usluga;

import java.util.List;


public interface IUslugaService {

    Usluga add(String naziv, String opis, Double cijena, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Usluga save(Long id, String naziv, String opis, Double cijena, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Usluga get(Long id);

    List<Usluga> findAll();
}
