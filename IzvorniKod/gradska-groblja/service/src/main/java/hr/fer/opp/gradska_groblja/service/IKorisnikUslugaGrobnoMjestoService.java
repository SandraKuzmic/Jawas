package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.KorisnikUslugaGrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Usluga;

import java.time.LocalDate;
import java.util.List;

public interface IKorisnikUslugaGrobnoMjestoService {

    KorisnikUslugaGrobnoMjesto add(Korisnik korisnik, Usluga usluga, GrobnoMjesto grobnoMjesto, LocalDate datumIzvrsenja, String korisnikKreiranoIzmjena);

    KorisnikUslugaGrobnoMjesto save(Long id, Korisnik korisnik, Usluga usluga, GrobnoMjesto grobnoMjesto, LocalDate datumIzvrsenja,
                                    String korisnikKreiranoIzmjena);

    KorisnikUslugaGrobnoMjesto get(Long id);

    List<KorisnikUslugaGrobnoMjesto> findAll();
}
