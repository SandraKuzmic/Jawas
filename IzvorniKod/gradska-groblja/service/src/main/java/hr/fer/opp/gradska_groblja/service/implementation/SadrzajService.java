package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.SadrzajRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Sadrzaj;
import hr.fer.opp.gradska_groblja.model.enums.VrstaSadrzaja;
import hr.fer.opp.gradska_groblja.service.ISadrzajService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SadrzajService implements ISadrzajService {
    private final SadrzajRepository sadrzajRepository;

    @Autowired
    public SadrzajService(SadrzajRepository sadrzajRepository) {
        this.sadrzajRepository = sadrzajRepository;
    }

    @Override
    public Sadrzaj add(VrstaSadrzaja vrsta, String lokacija, Groblje groblje, String korisnikKreiranoIzmjena) {

        this.checkPrerequisites(vrsta, lokacija);
        return sadrzajRepository.save(new Sadrzaj(vrsta, lokacija, groblje, LocalDateTime.now(), korisnikKreiranoIzmjena, LocalDateTime.now(),
                korisnikKreiranoIzmjena));
    }

    @Override
    public Sadrzaj save(Long id, VrstaSadrzaja vrsta, String lokacija, Groblje groblje, String korisnikIzmjena) {

        this.checkPrerequisites(vrsta, lokacija);
        LocalDateTime kreirano = sadrzajRepository.getOne(id).getKreirano();
        String korisnikKreirano = sadrzajRepository.getOne(id).getKorisnikKreirano();
        return sadrzajRepository.save(new Sadrzaj(vrsta, lokacija, groblje, kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Sadrzaj get(Long id) {
        return sadrzajRepository.getOne(id);
    }

    @Override
    public List<Sadrzaj> findAll() {
        return sadrzajRepository.findAll();
    }

    private void checkPrerequisites(VrstaSadrzaja vrsta, String lokacija) {
        if (vrsta == null) {
            throw new IllegalArgumentException("Vrsta can't be null or empty");
        }
        if (lokacija == null || lokacija.trim().isEmpty()) {
            throw new IllegalArgumentException("Lokacija can't be null or empty");
        }
    }

}
