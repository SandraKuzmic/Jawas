package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.UslugaRepository;
import hr.fer.opp.gradska_groblja.model.Usluga;
import hr.fer.opp.gradska_groblja.service.IUslugaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UslugaService implements IUslugaService {
    private final UslugaRepository uslugaRepository;

    @Autowired
    public UslugaService(UslugaRepository uslugaRepository) {
        this.uslugaRepository = uslugaRepository;
    }

    @Override
    public Usluga add(String naziv, String opis, Double cijena, String korisnikKreiranoIzmjena) {

        this.checkPrerequisites(naziv, opis, cijena);
        return uslugaRepository.save(new Usluga(naziv, opis, cijena, LocalDateTime.now(), korisnikKreiranoIzmjena, LocalDateTime.now(),
                korisnikKreiranoIzmjena));
    }

    @Override
    public Usluga save(Long id, String naziv, String opis, Double cijena, String korisnikIzmjena) {

        this.checkPrerequisites(naziv, opis, cijena);
        LocalDateTime kreirano = uslugaRepository.getOne(id).getKreirano();
        String korisnikKreirano = uslugaRepository.getOne(id).getKorisnikKreirano();
        return uslugaRepository.save(new Usluga(id, naziv, opis, cijena, kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Usluga get(Long id) {
        return uslugaRepository.getOne(id);
    }

    @Override
    public List<Usluga> findAll() {
        return uslugaRepository.findAll();
    }

    private void checkPrerequisites(String naziv, String opis, Double cijena) {

        if (naziv == null || naziv.trim().isEmpty()) {
            throw new IllegalArgumentException("Naziv can't be null or empty");
        }
        if (opis == null || opis.trim().isEmpty()) {
            throw new IllegalArgumentException("Opis can't be null or empty");
        }
        if (cijena == null) {
            throw new IllegalArgumentException("Cijena can't be null or empty");
        }
        if (cijena < 0) {
            throw new IllegalArgumentException("Cijena can't be negative value");
        }
    }
}
