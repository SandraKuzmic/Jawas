package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Licnost;

import java.time.LocalDate;
import java.util.List;

public interface ILicnostService {

    Licnost add(String ime, String prezime, LocalDate datumRodenja,
                LocalDate datumSmrti, LocalDate datumUkopa, GrobnoMjesto grobnoMjesto, String slika, String opis,
                String korisnikKreiranoIzmjena) throws IllegalAccessException ;

    Licnost save(Long id, String ime, String prezime, LocalDate datumRodenja,
                 LocalDate datumSmrti, LocalDate datumUkopa, GrobnoMjesto grobnoMjesto , String slika, String opis,
                 String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Licnost get (Long id) ;

    List<Licnost> findAll ();
}
