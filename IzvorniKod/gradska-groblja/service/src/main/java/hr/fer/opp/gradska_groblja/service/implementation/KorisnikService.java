package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.KorisnikRepository;
import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.enums.Uloga;
import hr.fer.opp.gradska_groblja.service.IKorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class KorisnikService implements IKorisnikService {

    private final KorisnikRepository korisnikRepository;
    private final int passwordMinLength;
    private final int oibLength;

    @Autowired
    public KorisnikService(KorisnikRepository korisnikRepository, @Value("${limits.password.min_length}") int passwordMinLength,
                           @Value("${limits.oib.length}") int oibLength) {
        this.korisnikRepository = korisnikRepository;
        this.passwordMinLength = passwordMinLength;
        this.oibLength = oibLength;
    }

    @Override
    public Korisnik add(String oib, String imeKorisnik, String prezimeKorisnik, String korisnickoIme, String lozinka, Uloga uloga,
                        String korisnikKreiranoIzmjena) {
        if (oib == null || oib.trim().length() != oibLength)
            throw new IllegalArgumentException("OIB is invalid");

        if (imeKorisnik == null || imeKorisnik.trim().isEmpty())
            throw new IllegalArgumentException("User name can not be null or empty");

        if (prezimeKorisnik == null || prezimeKorisnik.trim().isEmpty())
            throw new IllegalArgumentException("User surnamename can not be null or empty");

        if (korisnickoIme == null || korisnickoIme.trim().isEmpty())
            throw new IllegalArgumentException("Username can not be null or empty");

        if (lozinka == null || lozinka.trim().isEmpty())
            throw new IllegalArgumentException("Password can not be null or empty");

        if (lozinka.trim().length() < passwordMinLength)
            throw new IllegalArgumentException("Password must be at least " + passwordMinLength + " characters long");

        if (uloga == null)
            uloga = Uloga.KORISNIK;

        return korisnikRepository.save(new Korisnik(oib, imeKorisnik, prezimeKorisnik, korisnickoIme, lozinka, uloga, LocalDateTime.now(),
                korisnikKreiranoIzmjena, LocalDateTime.now(), korisnikKreiranoIzmjena));
    }

    @Override
    public Korisnik save(Long id, String oib, String imeKorisnik, String prezimeKorisnik, String korisnickoIme, String lozinka, Uloga uloga,
                         String korisnikIzmjena) {
        if (oib == null || oib.trim().length() != oibLength)
            throw new IllegalArgumentException("OIB is invalid");

        if (imeKorisnik == null || imeKorisnik.trim().isEmpty())
            throw new IllegalArgumentException("User name can not be null or empty");

        if (prezimeKorisnik == null || prezimeKorisnik.trim().isEmpty())
            throw new IllegalArgumentException("User surnamename can not be null or empty");

        if (korisnickoIme == null || korisnickoIme.trim().isEmpty())
            throw new IllegalArgumentException("Username can not be null or empty");

        if (lozinka == null || lozinka.trim().isEmpty())
            throw new IllegalArgumentException("Password can not be null or empty");

        if (lozinka.trim().length() < passwordMinLength)
            throw new IllegalArgumentException("Password must be at least " + passwordMinLength + " characters long");

        if (uloga == null)
            uloga = Uloga.KORISNIK;

        LocalDateTime kreirano = korisnikRepository.getOne(id).getKreirano();
        String korisnikKreirano = korisnikRepository.getOne(id).getKorisnikKreirano();
        return korisnikRepository.save(new Korisnik(id, oib, imeKorisnik, prezimeKorisnik, korisnickoIme, lozinka, uloga, kreirano, korisnikKreirano,
                LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Korisnik get(Long id) {
        return korisnikRepository.getOne(id);
    }

    @Override
    public Optional<Korisnik> findByKorisnickoIme(String korisnickoIme) {
        return korisnikRepository.findByKorisnickoIme(korisnickoIme);
    }

    @Override
    public List<Korisnik> findAll() {
        return korisnikRepository.findAll();
    }

}
