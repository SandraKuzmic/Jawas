package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Krajolik;

import java.util.List;

public interface IKrajolikService {

    Krajolik add (String naziv, String opis, String slika, String lokacija, Groblje groblje,
                  String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Krajolik save(Long id, String naziv, String opis, String slika, String lokacija, Groblje groblje,
                  String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Krajolik get(Long id);

    List<Krajolik> findAll ();
}
