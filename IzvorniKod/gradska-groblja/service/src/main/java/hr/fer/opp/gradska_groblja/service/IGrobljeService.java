package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Groblje;

import java.time.LocalTime;
import java.util.List;

public interface IGrobljeService {

    Groblje add (String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo,
                 LocalTime vikendOd, LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo,
                 String slika, String karta, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Groblje save (Long id, String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo,
                  LocalTime vikendOd, LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo, String slika, String karta,
                  String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Groblje get(Long id);

    List<Groblje> findAll();
}
