package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.enums.Uloga;

import java.util.List;
import java.util.Optional;

public interface IKorisnikService {
    Korisnik add(String oib, String imeKorisnik, String prezimeKorisnik, String korisnickoIme, String lozinka, Uloga uloga,
                 String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Korisnik save(Long id, String oib, String imeKorisnik, String prezimeKorisnik, String korisnickoIme, String lozinka, Uloga uloga,
                  String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Korisnik get(Long id);

    Optional<Korisnik> findByKorisnickoIme(String korisnickoIme);

    List<Korisnik> findAll();
}
