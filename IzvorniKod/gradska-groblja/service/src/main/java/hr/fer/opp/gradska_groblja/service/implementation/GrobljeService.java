package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.GrobljeRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.service.IGrobljeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Service
public class GrobljeService implements IGrobljeService {
    private final GrobljeRepository grobljeRepository;

    @Autowired
    public GrobljeService(GrobljeRepository grobljeRepository) {
        this.grobljeRepository = grobljeRepository;
    }

    @Override
    public Groblje add(String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo,
                       LocalTime vikendOd, LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo, String slika,
                       String karta, String korisnikKreiranoIzmjena) {

        this.checkPrerequisites(naziv, adresa, telefon, url, email, radniDanOd, radniDanDo,
                vikendOd, vikendDo, prijemPokojnikaOd,  prijemPokojnikaDo, slika, karta);

        return grobljeRepository.save(new Groblje(naziv, adresa, telefon, url, email, radniDanOd, radniDanDo, vikendOd, vikendDo,
                prijemPokojnikaOd, prijemPokojnikaDo, slika, karta,  LocalDateTime.now(), korisnikKreiranoIzmjena,
                LocalDateTime.now(), korisnikKreiranoIzmjena));
    }

    @Override
    public Groblje save(Long id, String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo,
                        LocalTime vikendOd, LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo,
                        String slika, String karta, String korisnikIzmjena) {

        this.checkPrerequisites(naziv, adresa, telefon, url, email, radniDanOd, radniDanDo,
                vikendOd, vikendDo, prijemPokojnikaOd,  prijemPokojnikaDo, slika, karta);
        LocalDateTime kreirano = grobljeRepository.getOne(id).getKreirano();
        String korisnikKreirano = grobljeRepository.getOne(id).getKorisnikKreirano();
        return grobljeRepository.save(new Groblje(id, naziv, adresa, telefon, url, email, radniDanOd, radniDanDo, vikendOd, vikendDo,
                prijemPokojnikaOd, prijemPokojnikaDo, slika, karta,
                kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Groblje get(Long id) {
        return grobljeRepository.getOne(id);
    }

    @Override
    public List<Groblje> findAll() {
        return grobljeRepository.findAll();
    }

    private void checkPrerequisites (String naziv, String adresa, String telefon, String url, String email, LocalTime radniDanOd, LocalTime radniDanDo,
                                     LocalTime vikendOd, LocalTime vikendDo, LocalTime prijemPokojnikaOd, LocalTime prijemPokojnikaDo, String slika, String karta) {
        if (naziv == null || naziv.trim().isEmpty()) {
            throw new IllegalArgumentException("Naziv can't be null or empty");
        }
        if (adresa == null || adresa.trim().isEmpty()) {
            throw new IllegalArgumentException("Adresa can't be null or empty");
        }
        if (telefon == null || telefon.trim().isEmpty()) {
            throw new IllegalArgumentException("Telefon can't be null or empty");
        }
        if (url == null || url.trim().isEmpty()) {
            throw new IllegalArgumentException("URL can't be null or empty");
        }
        if (email == null || email.trim().isEmpty()) {
            throw new IllegalArgumentException("E-mail can't be null or empty");
        }
        if (radniDanOd == null) {
            throw new IllegalArgumentException("Vrijeme otvaranja na radni dan can't be null or empty");
        }
        if (radniDanDo == null) {
            throw new IllegalArgumentException("Vrijeme zatvaranja na radni dan can't be null or empty");
        }
        if (vikendOd == null) {
            throw new IllegalArgumentException("Vrijeme otvaranja za vikend can't be null or empty");
        }
        if (vikendDo == null) {
            throw new IllegalArgumentException("Vrijeme zatvaranja za vikend can't be null or empty");
        }
        if (prijemPokojnikaOd == null) {
            throw new IllegalArgumentException("Vrijeme početka prijema pokojnika can't be null or empty");
        }
        if (prijemPokojnikaDo == null) {
            throw new IllegalArgumentException("Vrijeme završetka prijema pokojnik can't be null or empty");
        }
        if (slika == null || slika.trim().isEmpty()) {
            throw new IllegalArgumentException("Slika Groblja (URL) can't be null or empty");
        }
        if (karta == null || karta.trim().isEmpty()) {
            throw new IllegalArgumentException("Karta Groblja (URL) can't be null or empty");
        }
        if (radniDanOd.compareTo(radniDanDo)>=0) {
            throw new IllegalArgumentException("RadniDanDo can`t be smaller than RadniDanOd");
        }
        if (vikendOd.compareTo(vikendDo)>=0) {
            throw new IllegalArgumentException("VikendDo can`t be smaller than VikendOd");
        }
        if (prijemPokojnikaOd.compareTo(prijemPokojnikaDo)>=0) {
            throw new IllegalArgumentException("PrijemPokojnikaDo can`t be smaller than PrijemPokojnikaOd");
        }
    }
}
