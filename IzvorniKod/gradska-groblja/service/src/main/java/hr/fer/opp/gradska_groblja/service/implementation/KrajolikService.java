package hr.fer.opp.gradska_groblja.service.implementation;

import hr.fer.opp.gradska_groblja.dao.KrajolikRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Krajolik;
import hr.fer.opp.gradska_groblja.service.IKrajolikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class KrajolikService implements IKrajolikService {
    private final KrajolikRepository krajolikRepository;

    @Autowired
    public KrajolikService(KrajolikRepository krajolikRepository) {
        this.krajolikRepository = krajolikRepository;
    }

    @Override
    public Krajolik add(String naziv, String opis, String slika, String lokacija, Groblje groblje, String korisnikKreiranoIzmjena) {
        this.checkPrerequisites(naziv, lokacija);
        return krajolikRepository.save(new Krajolik(naziv, opis, slika, lokacija, groblje, LocalDateTime.now(), korisnikKreiranoIzmjena, LocalDateTime.now(),
                korisnikKreiranoIzmjena));
    }

    @Override
    public Krajolik save(Long id, String naziv, String opis, String slika, String lokacija, Groblje groblje, String korisnikIzmjena) {
        this.checkPrerequisites(naziv, lokacija);
        LocalDateTime kreirano = krajolikRepository.getOne(id).getKreirano();
        String korisnikKreirano = krajolikRepository.getOne(id).getKorisnikKreirano();
        return krajolikRepository.save(new Krajolik(id, naziv, opis, slika, lokacija, groblje, kreirano, korisnikKreirano, LocalDateTime.now(), korisnikIzmjena));
    }

    @Override
    public Krajolik get(Long id) {
        return krajolikRepository.getOne(id);
    }

    @Override
    public List<Krajolik> findAll() {
        return krajolikRepository.findAll();
    }


    private void checkPrerequisites(String naziv, String lokacija) {
        if (naziv == null || naziv.trim().isEmpty()) {
            throw new IllegalArgumentException("Naziv can't be null or empty");
        }
        if (lokacija == null || lokacija.trim().isEmpty()) {
            throw new IllegalArgumentException("Lokacija can't be null or empty");
        }
    }
}
