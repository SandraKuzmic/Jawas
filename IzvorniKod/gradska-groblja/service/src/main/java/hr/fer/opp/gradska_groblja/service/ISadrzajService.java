package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Sadrzaj;
import hr.fer.opp.gradska_groblja.model.enums.VrstaSadrzaja;

import java.util.List;


public interface ISadrzajService {

    Sadrzaj add(VrstaSadrzaja vrsta, String lokacija, Groblje groblje, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Sadrzaj save(Long id, VrstaSadrzaja vrsta, String lokacija, Groblje groblje, String korisnikKreiranoIzmjena) throws IllegalAccessException;

    Sadrzaj get(Long id);

    List<Sadrzaj> findAll();
}
