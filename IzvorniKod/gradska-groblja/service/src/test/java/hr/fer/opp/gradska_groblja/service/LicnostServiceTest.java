package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.LicnostRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.GrobnoMjesto;
import hr.fer.opp.gradska_groblja.model.Licnost;
import hr.fer.opp.gradska_groblja.service.implementation.LicnostService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class LicnostServiceTest {
    private LicnostService licnostService;

    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";
    private static final Groblje GROBLJE = new Groblje("Mirogoj", "Aleja Hermanna Bollea 27", "01/4576234", "mirogoj.hr",
            "mirogoj@groblja.hr", LocalTime.of(0, 0), LocalTime.of(8, 0), LocalTime.of(0, 0),
            LocalTime.of(4, 0), LocalTime.of(0, 0), LocalTime.of(6, 0),
            "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg",
            "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg", KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static final String IME = "Josip";
    private static final String PREZIME = "Broz";
    private static final LocalDate DATUM_RODENJA = LocalDate.of(1892, 5, 7);
    private static final LocalDate DATUM_SMRTI = LocalDate.of(1980, 5, 4);
    private static final LocalDate DATUM_UKOPA = LocalDate.of(1980, 5, 6);
    private static final GrobnoMjesto GROBNO_MJESTO = new GrobnoMjesto("1A", (short) 1, (short) 1, 1, GROBLJE,
            KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static final String SLIKA = "https://i0.wp.com/hu-benedikt.hr/wp-content/uploads/2015/02/Tito04_nnn_obz_221211.jpg";
    private static final String OPIS = "Diktator";

    private static Licnost LICNOST(String ime, String prezime, LocalDate datumRodenja,
                                   LocalDate datumSmrti, LocalDate datumUkopa, GrobnoMjesto grobnoMjesto, String slika, String opis){
        return new Licnost(ime, prezime, datumRodenja, datumSmrti, datumUkopa, grobnoMjesto, slika, opis, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private LicnostRepository licnostRepository;

    @Before
    @Autowired
    public void setUp(){
        this.licnostService = new LicnostService(licnostRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(licnostRepository.save(any(Licnost.class)))
                .thenReturn(LICNOST(IME, PREZIME, DATUM_RODENJA, DATUM_SMRTI, DATUM_UKOPA, GROBNO_MJESTO, SLIKA, OPIS));
        Licnost result = licnostService.add(IME, PREZIME, DATUM_RODENJA, DATUM_SMRTI, DATUM_UKOPA, GROBNO_MJESTO, SLIKA, OPIS, KORISNIK_KREIRANO);
        Licnost expected = new Licnost(IME, PREZIME, DATUM_RODENJA, DATUM_SMRTI, DATUM_UKOPA, GROBNO_MJESTO, SLIKA, OPIS, KREIRANO, KORISNIK_KREIRANO,
                IZMJENA, KORISNIK_IZMJENA);
        licnostEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDatumSmrtiGreaterThenDatumRodenja() {
        Mockito.when(licnostRepository.save(any(Licnost.class)))
                .thenReturn(LICNOST(IME, PREZIME, DATUM_RODENJA, LocalDate.of(952,1,1), DATUM_UKOPA, GROBNO_MJESTO, SLIKA, OPIS));
        licnostService.add(IME, PREZIME, DATUM_RODENJA, LocalDate.of(952,1,1), DATUM_UKOPA, GROBNO_MJESTO, SLIKA, OPIS, KORISNIK_KREIRANO);
    }

    private void licnostEquals (Licnost expected, Licnost result) {
        assertEquals(expected.getIme(), result.getIme());
        assertEquals(expected.getPrezime(), result.getPrezime());
        assertEquals(expected.getDatumRodenja(), result.getDatumRodenja());
        assertEquals(expected.getDatumSmrti(), result.getDatumSmrti());
        assertEquals(expected.getDatumUkopa(), result.getDatumUkopa());
        assertEquals(expected.getGrobnoMjesto(), result.getGrobnoMjesto());
        assertEquals(expected.getSlika(), result.getSlika());
        assertEquals(expected.getOpis(), result.getOpis());
    }
}
