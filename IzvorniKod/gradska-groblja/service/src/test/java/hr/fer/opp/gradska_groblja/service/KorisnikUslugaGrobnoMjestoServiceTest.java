package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.KorisnikUslugaGrobnoMjestoRepository;
import hr.fer.opp.gradska_groblja.model.*;
import hr.fer.opp.gradska_groblja.model.enums.Uloga;
import hr.fer.opp.gradska_groblja.service.implementation.KorisnikUslugaGrobnoMjestoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class KorisnikUslugaGrobnoMjestoServiceTest {
    private KorisnikUslugaGrobnoMjestoService korisnikUslugaGrobnoMjestoService;

    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";
    private static final Groblje GROBLJE = new Groblje("Mirogoj", "Aleja Hermanna Bollea 27", "01/4576234", "mirogoj.hr",
            "mirogoj@groblja.hr", LocalTime.of(0, 0), LocalTime.of(8, 0), LocalTime.of(0, 0),
            LocalTime.of(4, 0), LocalTime.of(0, 0), LocalTime.of(6, 0),
            "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg",
            "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg", KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static final Korisnik KORISNIK = new Korisnik("12345678901", "Marina", "Šešerko", "mSeserko", "Palmos467",
            Uloga.ADMIN, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static final Usluga USLUGA = new Usluga("Pogreb", "pogreb", 8467.56, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);

    private static final GrobnoMjesto GROBNO_MJESTO = new GrobnoMjesto("1A", (short) 1, (short) 1, 1, GROBLJE,
            KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static final LocalDate DATUM_IZVRSENJA = LocalDate.now();

    private static KorisnikUslugaGrobnoMjesto KORISNIK_USLUGA_GROBNO_MJESTO(Korisnik korisnik, Usluga usluga, GrobnoMjesto grobnoMjesto,
                                                                            LocalDate datumIzvrsenja) {
        return new KorisnikUslugaGrobnoMjesto(korisnik, usluga, grobnoMjesto, datumIzvrsenja, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private KorisnikUslugaGrobnoMjestoRepository korisnikUslugaGrobnoMjestoRepository;

    @Before
    @Autowired
    public void setUp(){
        this.korisnikUslugaGrobnoMjestoService = new KorisnikUslugaGrobnoMjestoService(korisnikUslugaGrobnoMjestoRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(korisnikUslugaGrobnoMjestoRepository.save(any(KorisnikUslugaGrobnoMjesto.class)))
                .thenReturn(KORISNIK_USLUGA_GROBNO_MJESTO(KORISNIK, USLUGA, GROBNO_MJESTO, DATUM_IZVRSENJA));
        KorisnikUslugaGrobnoMjesto result = korisnikUslugaGrobnoMjestoService.add(KORISNIK, USLUGA, GROBNO_MJESTO, DATUM_IZVRSENJA, KORISNIK_KREIRANO);
        KorisnikUslugaGrobnoMjesto expected = new KorisnikUslugaGrobnoMjesto(KORISNIK, USLUGA, GROBNO_MJESTO, DATUM_IZVRSENJA
                , KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        korisnikUslugaGrobnoMjestoEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyKorisnik() {
        Mockito.when(korisnikUslugaGrobnoMjestoRepository.save(any(KorisnikUslugaGrobnoMjesto.class)))
                .thenReturn(KORISNIK_USLUGA_GROBNO_MJESTO(null, USLUGA, GROBNO_MJESTO, DATUM_IZVRSENJA));
        korisnikUslugaGrobnoMjestoService.add(null, USLUGA, GROBNO_MJESTO, DATUM_IZVRSENJA, KORISNIK_KREIRANO);
    }

    private void korisnikUslugaGrobnoMjestoEquals (KorisnikUslugaGrobnoMjesto expected, KorisnikUslugaGrobnoMjesto result) {
        assertEquals(expected.getKorisnik(), result.getKorisnik());
        assertEquals(expected.getUsluga(), result.getUsluga());
        assertEquals(expected.getGrobnoMjesto(), result.getGrobnoMjesto());
        assertEquals(expected.getDatumIzvrsenja(), result.getDatumIzvrsenja());
    }
}
