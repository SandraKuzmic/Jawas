package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.UslugaRepository;
import hr.fer.opp.gradska_groblja.model.Usluga;
import hr.fer.opp.gradska_groblja.service.implementation.UslugaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class UslugaServiceTest {
    private UslugaService uslugaService;

    private static final String NAZIV = "Pogreb";
    private static final String OPIS = "Pogrebna usluga";
    private static final Double CIJENA = (double)6253.42;
    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";

    private static Usluga USLUGA(String naziv, String opis, Double cijena){
        return new Usluga(naziv, opis, cijena, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private UslugaRepository uslugaRepository;

    @Before
    @Autowired
    public void setUp(){
        this.uslugaService = new UslugaService(uslugaRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(uslugaRepository.save(any(Usluga.class)))
                .thenReturn(USLUGA(NAZIV, OPIS, CIJENA));
        Usluga result = uslugaService.add(NAZIV, OPIS, CIJENA, KORISNIK_KREIRANO);
        Usluga expected = new Usluga(NAZIV, OPIS, CIJENA, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        uslugaEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyNaziv() {
        Mockito.when(uslugaRepository.save(any(Usluga.class)))
                .thenReturn(USLUGA("", OPIS, CIJENA));
        uslugaService.add("", OPIS, CIJENA, KORISNIK_KREIRANO);
    }

    private void uslugaEquals (Usluga expected, Usluga result) {
        assertEquals(expected.getNaziv(), result.getNaziv());
        assertEquals(expected.getOpis(), result.getOpis());
        assertEquals(expected.getCijena(), result.getCijena());
    }
}
