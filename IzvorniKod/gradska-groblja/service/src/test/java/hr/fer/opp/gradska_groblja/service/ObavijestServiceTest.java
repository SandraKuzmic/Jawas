package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.ObavijestRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Obavijest;
import hr.fer.opp.gradska_groblja.service.implementation.ObavijestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class ObavijestServiceTest {
    private ObavijestService obavijestService;

    private static final String NASLOV = "Pogreb";
    private static final String TEKST = "Pogreb svima nam dragog Ive Smradanera";
    private static final String SLIKA = "https://www.24sata.hr/media/img/cd/42/f0292a8b5a86b43c875e.jpeg";
    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";
    private static final Groblje GROBLJE = new Groblje("Mirogoj", "Aleja Hermanna Bollea 27", "01/4576234", "mirogoj.hr",
            "mirogoj@groblja.hr", LocalTime.of(0, 0), LocalTime.of(8, 0), LocalTime.of(0, 0),
            LocalTime.of(4, 0), LocalTime.of(0, 0), LocalTime.of(6, 0),
            "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg",
            "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg", KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    private static Obavijest OBAVIJEST(String naslov, String tekst, String slika, Groblje groblje){
        return new Obavijest(naslov, tekst, slika, groblje, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private ObavijestRepository obavijestRepository;

    @Before
    @Autowired
    public void setUp(){
        this.obavijestService = new ObavijestService(obavijestRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(obavijestRepository.save(any(Obavijest.class)))
                .thenReturn(OBAVIJEST(NASLOV, TEKST, SLIKA, GROBLJE));
        Obavijest result = obavijestService.add(NASLOV, TEKST, SLIKA, GROBLJE, KORISNIK_KREIRANO);
        Obavijest expected = new Obavijest(NASLOV, TEKST, SLIKA, GROBLJE, KREIRANO, KORISNIK_KREIRANO,
                IZMJENA, KORISNIK_IZMJENA);
        obavijestEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyGroblje() {
        Mockito.when(obavijestRepository.save(any(Obavijest.class)))
                .thenReturn(OBAVIJEST(NASLOV, TEKST, SLIKA, null));
        obavijestService.add(NASLOV, TEKST, SLIKA, null, KORISNIK_KREIRANO);
    }

    private void obavijestEquals (Obavijest expected, Obavijest result) {
        assertEquals(expected.getNaslov(), result.getNaslov());
        assertEquals(expected.getTekst(), result.getTekst());
        assertEquals(expected.getSlika(), result.getSlika());
        assertEquals(expected.getGroblje(), result.getGroblje());
    }
}
