package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.SadrzajRepository;
import hr.fer.opp.gradska_groblja.model.Groblje;
import hr.fer.opp.gradska_groblja.model.Sadrzaj;
import hr.fer.opp.gradska_groblja.model.enums.VrstaSadrzaja;
import hr.fer.opp.gradska_groblja.service.implementation.SadrzajService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class SadrzajServiceTest {
    private SadrzajService sadrzajService;

    private static final VrstaSadrzaja VRSTA_SADRZAJA = VrstaSadrzaja.KONTEJNER;
    private static final String LOKACIJA = "68.1.19, 45.34.2";
    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "mseserko";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "mjukic";
    private static final Groblje GROBLJE = new Groblje("Mirogoj", "Aleja Hermanna Bollea 27", "01/4576234", "mirogoj.hr",
            "mirogoj@groblja.hr", LocalTime.of(0, 0), LocalTime.of(8, 0), LocalTime.of(0, 0),
            LocalTime.of(4, 0), LocalTime.of(0, 0), LocalTime.of(6, 0),
            "http://www.gradskagroblja.hr/UserDocsImages/galerije/mirogoj/GLAVNI-ULAZ-1.jpg",
            "http://www.gradskagroblja.hr/UserDocsImages///mirogoj-stari.jpg", KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);

    private static Sadrzaj SADRZAJ(VrstaSadrzaja vrsta, String lokacija, Groblje groblje){
        return new Sadrzaj(vrsta, lokacija, groblje, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private SadrzajRepository sadrzajRepository;

    @Before
    @Autowired
    public void setUp(){
        this.sadrzajService = new SadrzajService(sadrzajRepository);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(sadrzajRepository.save(any(Sadrzaj.class)))
                .thenReturn(SADRZAJ(VRSTA_SADRZAJA, LOKACIJA, GROBLJE));
        Sadrzaj result = sadrzajService.add(VRSTA_SADRZAJA, LOKACIJA, GROBLJE, KORISNIK_KREIRANO);
        Sadrzaj expected = new Sadrzaj(VRSTA_SADRZAJA, LOKACIJA, GROBLJE, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        sadrzajEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyLokacija() {
        Mockito.when(sadrzajRepository.save(any(Sadrzaj.class)))
                .thenReturn(SADRZAJ(VRSTA_SADRZAJA, "", GROBLJE));
        sadrzajService.add(VRSTA_SADRZAJA, "", GROBLJE, KORISNIK_KREIRANO);
    }

    private void sadrzajEquals (Sadrzaj expected, Sadrzaj result) {
        assertEquals(expected.getVrsta(), result.getVrsta());
        assertEquals(expected.getLokacija(), result.getLokacija());
        assertEquals(expected.getGroblje(), result.getGroblje());
    }
}
