package hr.fer.opp.gradska_groblja.service;

import hr.fer.opp.gradska_groblja.dao.KorisnikRepository;
import hr.fer.opp.gradska_groblja.model.Korisnik;
import hr.fer.opp.gradska_groblja.model.enums.Uloga;
import hr.fer.opp.gradska_groblja.service.implementation.KorisnikService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class KorisnikServiceTest {
    private KorisnikService korisniciService;

    private static final String OIB = "12345678910";
    private static final String IME = "Dora";
    private static final String PREZIME = "Horvat";
    private static final String USERNAME = "dHorvat";
    private static final String PASSWORD = "Palmos46";
    private static final Uloga ULOGA = Uloga.ADMIN;
    private static final LocalDateTime KREIRANO = LocalDateTime.now();
    private static final String KORISNIK_KREIRANO = "lkristof";
    private static final LocalDateTime IZMJENA = LocalDateTime.now();
    private static final String KORISNIK_IZMJENA = "lkristof";


    private static final int PASSWORD_MIN_LENGTH = 5;
    private static final int OIB_LENGTH = 11;

    private static Korisnik KORISNIK(String oib, String imeKorisnik, String prezimeKorisnik, String korisnickoIme, String lozinka, Uloga uloga){
        return new Korisnik(oib, imeKorisnik,prezimeKorisnik,korisnickoIme,lozinka,uloga, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
    }

    @Mock
    private KorisnikRepository korisnikRepository;

    @Before
    @Autowired
    public void setUp(){
        this.korisniciService = new KorisnikService(korisnikRepository, PASSWORD_MIN_LENGTH, OIB_LENGTH);
    }

    @Test
    public void testAddValidData() {
        Mockito.when(korisnikRepository.save(any(Korisnik.class)))
                .thenReturn(KORISNIK(OIB, IME, PREZIME,USERNAME,PASSWORD,ULOGA));
        Korisnik result = korisniciService.add(OIB, IME, PREZIME,USERNAME,PASSWORD,ULOGA, KORISNIK_KREIRANO);
        Korisnik expected = new Korisnik(OIB, IME, PREZIME,USERNAME,PASSWORD,ULOGA, KREIRANO, KORISNIK_KREIRANO, IZMJENA, KORISNIK_IZMJENA);
        korisniciEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyOib() {
        Mockito.when(korisnikRepository.save(any(Korisnik.class)))
                .thenReturn(KORISNIK(" ",IME, PREZIME,USERNAME,PASSWORD,ULOGA));
        korisniciService.add(" ",IME, PREZIME,USERNAME,PASSWORD,ULOGA,KORISNIK_KREIRANO);
    }

    private void korisniciEquals (Korisnik expected, Korisnik result) {
        assertEquals(expected.getOib(), result.getOib());
        assertEquals(expected.getIme(), result.getIme());
        assertEquals(expected.getPrezime(), result.getPrezime());
        assertEquals(expected.getKorisnickoIme(), result.getKorisnickoIme());
        assertEquals(expected.getLozinka(), result.getLozinka());
        assertEquals(expected.getUloga(), result.getUloga());
    }
}
