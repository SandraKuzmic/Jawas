# jawas
# Upgrade packages
sudo apt-get update
sudo apt-get upgrade

# Generate SSH key used to clone from GitLab
ssh-keygen -t rsa -b 4096 -C "jawas@jawas.group"
cat ~/.ssh/id_rsa.pub 

# Add printed key to user on GitLab

cd /var/www/
sudo usermod -aG www-data jawas 
sudo chown -R jawas:www-data .
git clone git@gitlab.com:SandraKuzmic/Jawas.git

sudo apt install default-jre
sudo apt install default-jdk
sudo apt install maven

cd Jawas/
git checkout development
cd IzvorniKod/gradska-groblja/
mvn package

# Test whether backend works
java -jar web/target/web-1.0-SNAPSHOT.jar --spring.profiles.active=prod

sudo apt-get install postgresql
sudo -u postgres createuser jawas
sudo -u postgres createdb gradska_groblja
sudo -u postgres psql
> alter user jawas with encrypted password 'jawas1234';
> grant all privileges on database gradska_groblja to jawas;

# Copy contents of gradska-groblja.service to:
sudo vim /etc/systemd/system/gradska-groblja.service

sudo systemctl daemon-reload
sudo systemctl start gradska-groblja.service 
sudo journalctl -f -u gradska-groblja.service


# Install npm and update it
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y build-essential
sudo npm i -g npm


cd ../web-ui/
npm i
npm run build
ls ./build


sudo apt install nginx

sudo ufw status
sudo ufw allow ssh
sudo ufw allow "Nginx Full"
sudo ufw enable
sudo ufw status

# Copy contents of nginx config file jawas.config to:
sudo vim /etc/nginx/sites-enabled/jawas.config

sudo service nginx restart
